<?php
	class Ussd_models extends CI_Model {
		function get_user($msisdn){
			$this->db->where('msisdn',$msisdn);
			$this->db->where('status',"1");
			return $this->db->get('user');
		}
        function get_user_password($msisdn){
            $this->db->select('pwd')
                ->from('user')
                ->where('msisdn',$msisdn);
            $query = $this->db->get();
            return $query->row()->pwd;
        }
        function get_user_national_id($msisdn){
            $this->db->select('id_number')
                ->from('user')
                ->where('msisdn',$msisdn);
            $query = $this->db->get();
            return $query->row()->id_number;
        }
		function add_user($msisdn,$id_number){
			$data=array(
				'msisdn'=>$msisdn,
				'id_number'=>$id_number,
				'status'=>"1",
				'pwd'=>"",
				);
			$this->db->insert('user',$data);
			
		}
		function add_pwd($msisdn,$pwd){
			$data=array(
				'pwd'=>$pwd,
			);
			$this->db->where('msisdn',$msisdn);
			$this->db->update('user',$data);
			$report = array();
		    $report['error'] = $this->db->error();
		    return $report;
		}
		function add_session($session_id,$msisdn,$menuid,$policy_no,$lob){
			$data=array(
				'session_id'=>$session_id,
				'msisdn'=>$msisdn,
				'menu_id'=>$menuid,
				'policy_no'=>$policy_no,
				'lob'=>$lob
				);
			$this->db->insert('session_tracker',$data);
			
		}
		function get_session($msisdn,$session_id){
			$this->db->where('msisdn',$msisdn);
			$this->db->where('session_id',$session_id);
			$this->db->limit(1);
			$this->db->order_by("Id","desc");
			return $this->db->get('session_tracker');
		}
		function get_menu($id){
			$this->db->where('Id',$id);
			return $this->db->get('menus');
		}
		function get_menu_items($policy_id){
			$this->db->where('policy_id',$policy_id);
			$this->db->where('status',"1");
			$this->db->order_by("Id","desc");
			return $this->db->get('policy_menus');
		}
        function add_comment($msisdn,$selection){
            $data=array(
                'msisdn'=>$msisdn,
                'text'=>$selection,
            );
            $this->db->insert('comments',$data);
        }
        function reset_pwd($msisdn,$pwd){
            $data=array(
                'pwd'=>$pwd,
            );
            $this->db->where('msisdn',$msisdn);
            $this->db->update('user',$data);
            $report = array();
            $report['error'] = $this->db->error();
            return $report;
        }
        function reset_national_id($msisdn,$national_id){
            $data=array(
                'id_number'=>$national_id,
            );
            $this->db->where('msisdn',$msisdn);
            $this->db->update('user',$data);
            $report = array();
            $report['error'] = $this->db->error();
            return $report;
        }
        function get_investment_rate_of_return($id){
            $this->db->select('*')
                ->from('investment_rate_of_retrun')
                ->where('id',$id);
            $query = $this->db->get();
            return $query->row();
        }
	}