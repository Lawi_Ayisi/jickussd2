<?php

class TokenManager extends CI_Model
{
    private $redis;
    private $api_server_url;
    private $api_username;
    private $api_password;
    private $api_client_secret;
    private $api_client_id;
    private $api_grant_type;

    public function __construct()
    {
        $this->api_server_url = $this->config->item('api_server_url');
        $this->api_username = $this->config->item('api_username');
        $this->api_password = $this->config->item('api_password');
        $this->api_client_secret = $this->config->item('api_client_secret');
        $this->api_client_id = $this->config->item('api_client_id');
        $this->api_grant_type = $this->config->item('api_grant_type');
    }
    public function getMsafiriToken()
    {
        require_once APPPATH . 'libraries/codeigniter-predis/src/Redis.php';
        $redis = new \CI_Predis\Redis(['serverName' => 'localhost']);
        $tokenfromredis = json_decode($redis->get('msafiri_token'));
        if (isset($tokenfromredis) && $tokenfromredis->access_token != null) {
            $token_time = $tokenfromredis->token_time;
            
            $current_time = date("Y-m-d H:i:s");
            $mathes = strtotime($current_time) - strtotime($token_time);
            if ($mathes < 3600) {
                return $tokenfromredis->access_token;
            } else {
                return $this->refreshToken($tokenfromredis->refresh_token);
            }
        } else {
            $token = $this->getNewToken();
            // echo $token;
            return $token;
        }
    }
    public function getNewToken()
    {
        require_once APPPATH . 'libraries/codeigniter-predis/src/Redis.php';
        $this->redis = new \CI_Predis\Redis(['serverName' => 'localhost']);

        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => $this->api_server_url."/oauth/token",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_SSL_VERIFYHOST => false,
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => false,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => array('grant_type' => 'password','client_id' => '2','client_secret' => 'eZyv5KUgZOwc3YMucg6ZMOfHWyMpbaEaoj9CaGQT','username' => 'msafiri@jubileekenya.com','password' => '5G37!r9$=vUF7_zY'),
        CURLOPT_HTTPHEADER => array(
            "Accept: application/json"
        ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $api_response_object = (Object)json_decode($response);
            $token_data = array(
                "token_time" => date('Y-m-d H:i:s'),
                "token_type" => $api_response_object->token_type,
                "access_token" => $api_response_object->access_token,
                "refresh_token" => $api_response_object->refresh_token,
            );
            // print_r($token_data);
            $this->redis->set('msafiri_token', json_encode($token_data));
            return $api_response_object->access_token;
        }
    }
    public function refreshToken($refresh_token)
    {
        require_once APPPATH . 'libraries/codeigniter-predis/src/Redis.php';
        $this->redis = new \CI_Predis\Redis(['serverName' => 'localhost']);
        $curl = curl_init();
        // echo $this->api_server_url;
        // die();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->api_server_url."/oauth/token",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => array('grant_type' => $this->api_grant_type, 'client_id' => $this->api_client_id, 'client_secret' => $this->api_client_secret, 'refresh_token' => $refresh_token),
            CURLOPT_HTTPHEADER => array(
                "Accept: application/json"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        // die();
        } else {
            $api_response_object = (Object)json_decode($response);
            $token_data = array(
                "token_time" => date('Y-m-d H:i:s'),
                "token_type" => $api_response_object->token_type,
                "access_token" => $api_response_object->access_token,
                "refresh_token" => $api_response_object->refresh_token,
            );
            // print_r($token_data);
            $this->redis->set('msafiri_token', json_encode($token_data));
            return $api_response_object->access_token;
        }
    }
}
