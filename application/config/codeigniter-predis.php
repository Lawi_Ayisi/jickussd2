<?php
defined('BASEPATH') or exit('No direct script access allowed');

$config['redis'] = [];

switch (ENVIRONMENT) {
    case 'development':
    case 'testing':
        $config['redis'] = [
            'default_server' => 'localhost',
            'servers' => [
                'localhost' => [
                    'scheme' => 'tcp',
                    'host' => 'localhost',
                    'port' => 63790,
                    'password' => null,
                    'database' => 0,
                ],
            ],
        ];
        break;
    case 'homologation':
        break;
    case 'production':
        $config['redis'] = [
                'default_server' => 'localhost',
                'servers' => [
                    'localhost' => [
                        'scheme' => 'tcp',
                        'host' => 'localhost',
                        'port' => 6379,
                        'password' => null,
                        'database' => 0,
                    ],
                ],
            ];

        break;
    default:
        throw new Exception('The application environment is not set correctly.');
}
