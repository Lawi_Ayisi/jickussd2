<?php
		/**
 * PHP swiftmailer lib for Codeigniter
 *
 * @package        	CodeIgniter
 * @subpackage    	Libraries
 * @category    	Libraries
 * @author	brian.otieno709@gmail.com
 * 
 * 
 * @version		1.0
 */
 
class CI_swiftmailer
{

	function __construct() {
		require_once 'swiftmailer/swift_required.php';
		$this->data = array(
            'username' => 'info@the-bluecompany.org',
            'password' => 'password'
        );
	}
	function sendmail($msg,$subject,$from,$name){
		$data = $this->data;
		try{
			// Create the Transport the call setUsername() and setPassword()
			$transport = Swift_SmtpTransport::newInstance('smtpout.secureserver.net', 3535)
			->setUsername($data['username'])->setPassword($data['password']);
			// Create the Mailer using your created Transport
			$mailer = Swift_Mailer::newInstance($transport);
			// Create the message
			$message = Swift_Message::newInstance()
			//anti flood
			
			// Give the message a subject
			->setSubject($subject)
			// Set the From address with an associative array
			->setFrom(array($from => $name))
			// Set the To addresses with an associative array
			->setTo(array('info@the-bluecompany.org' => 'The Blue Company'))
			// Give it a body
			->setBody($msg);
			// And optionally an alternative body
			$result=$mailer->send($message);
			$output="000";
		}catch(Exception $ex){
			$msg="Please try again later";
			$output="001";
		}
		return $output;
	}
}
?>