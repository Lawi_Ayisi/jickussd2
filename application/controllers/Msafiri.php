<?php

/**
 * Created by PhpStorm.
 * User: lawi
 * Date: 12/6/18
 * Time: 4:57 PM
 */
include("Welcome.php");
class Msafiri extends Welcome
{
    public $userIdnumber;
    public $numberOfpassengers;
    public $whereFrom;
    public $whereTo;
    public $busCompany;
    public $totalCost;

    public function __construct()
    {
        parent:: __construct();
        $this->load->database();
        // $this->load->model('Ussd_models');
        $this->load->model('TokenManager');
        // $this->load->library('CI_infobip');
        // $this->load->library('session');
    }

    public function index()
    {
        echo "Hi 3";
    }

    public function session()
    {
        header("Cache-Control: no-cache, must-revalidate");
        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        header('Content-type:application/json');
        header('Access-Control-Allow-Origin: *');
        $session_id = $this->uri->segment(3);
        $session_state = $this->uri->segment(4);
        $postdata = file_get_contents("php://input");
        $postdata = json_decode($postdata);
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $msisdn = $postdata->msisdn;
        } elseif ($_SERVER['REQUEST_METHOD'] === 'PUT') {
            $msisdn = $this->get_put_msisdn();
        }
        $response = array();
        switch ($session_state) {
            case "start":
                $userMSISDN = $this->get_put_msisdn();
                $userMobileNumber = substr($userMSISDN, 3);
                $this->Ussd_models->add_session($session_id, $msisdn, "2", "", "");
                $sent_response = $this->getmenu("47");
                echo $sent_response;
                break;

            case "response":
                $getsession = $this->Ussd_models->get_session($msisdn, $session_id);
                foreach ($getsession->result() as $sessionresult) {
                    $themenuid = $sessionresult->menu_id;
                    $selection = $this->get_put_text();

                    switch ($themenuid) {
                        case 1:
                                //Welcome menu
                                $this->Ussd_models->add_session($session_id, $msisdn, "2", "", "");
                                $sent_response = $this->getmenu("47");
                                echo $sent_response;
                            break;
                        case 2:
                            $numberOfpassengers = $selection;
                                $this->Ussd_models->add_session($session_id, $msisdn, "3", "", "");
                                $sent_response = $this->getmenu("48");
                                echo $sent_response;
                                // $msafiri_data = array();
                            $msafiri_data = array('session_id' => $session_id, 'phone_number' => $msisdn, 'number_of_passengers' => $selection);
                            $str = $this->db->insert('msafiri', $msafiri_data);
                                // $this->db->where('session_id', $session_id);
                                // $str = $this->db->update('msafiri', $msafiri_data);
                                
                            break;
                        case 3:
                            $whereFrom = $selection;
                                $this->Ussd_models->add_session($session_id, $msisdn, "4", "", "");
                                $sent_response = $this->getmenu("49");
                            echo $sent_response;
                            $msafiri_data = array('where_from' => $selection);
                            $this->db->where('session_id', $session_id);
                            $str = $this->db->update('msafiri', $msafiri_data);
                            break;
                        case 4:
                            $whereTo = $selection;
                                $this->Ussd_models->add_session($session_id, $msisdn, "5", "", "");
                                $sent_response = $this->getmenu("50");
                                echo $sent_response;
                            $msafiri_data = array('where_to' => $selection);
                            $this->db->where('session_id', $session_id);
                            $str = $this->db->update('msafiri', $msafiri_data);

                            break;
                        case 5:
                            $busCompany = $selection;
                            $this->Ussd_models->add_session($session_id, $msisdn, "6", "", "");
                            $results = $this->db->get_where('msafiri', array('session_id'=>$session_id));
                            $row = $results->result_array();
                            $numberOfpassengers = $row[0]["number_of_passengers"];
                            $totalCost = $numberOfpassengers*25;
                            $totalcosttext = "The total cost of the insurance will be: ".$totalCost. "/= . Reply with 1 to proceed ?" ;
                            $sent_response = $this->ussd_core(false, $totalcosttext, 200, "");
                            echo $sent_response;
                            $msafiri_data = array('bus_company' => $selection);
                            $this->db->where('session_id', $session_id);
                            $str = $this->db->update('msafiri', $msafiri_data);
                            
                            break;
                        case 6:
                            if ($selection == 1) {
                                $sent_response = $this->end("Please enter your pin to complete the mpesa payment, in the next screen after you close this menu.", "200");
                                // echo $sent_response;
                                
                                $results = $this->db->get_where('msafiri', array('session_id' => $session_id));
                                $row = $results->result_array();
                                $numberOfpassengers = $row[0]["number_of_passengers"];
                                $totalCost = $numberOfpassengers * 25;

                                $mdata = array(
                                    "phone_number" => $row[0]['phone_number'],
                                    "number_of_travellers" => $row[0]['number_of_passengers'],
                                    "travel_from" => $row[0]['where_from'],
                                    "travel_to" => $row[0]['where_to'],
                                    "bus_company" => $row[0]['bus_company']

                                );
                                
                                $mpesa_api = $this->msafiri_api(json_encode($mdata));
                                if ($mpesa_api == 0) {
                                    $mpesa_api = $this->msafiri_api(json_encode($mdata));
                                    if ($mpesa_api == 0) {
                                        $sent_response = $this->end("Payment services not available, please try again later", "200");
                                        echo $sent_response;
                                        break;
                                        die();
                                    }
                                }
                                echo $sent_response;
                                $apidata = (array)$mpesa_api;
                                $temp_array = array_merge($apidata, array('session_id'=>$session_id));
                                $this->db->insert('msafiri_api', $temp_array);
                                $account_data = json_encode(array('account_number'=>$mpesa_api->mpesa_account));
                                $stkdata = $this->mpesa_stk($account_data);
                                // print_r($stkdata);
                                $stk_transaction = [
                                    "request_time" => $stkdata->request_time,
                                    "response_time" => $stkdata->response_time,
                                    "status" =>$stkdata->status,
                                    "message" => $stkdata->message,
                                    "MerchantRequestID" => $stkdata->stk->MerchantRequestID,
                                    "CheckoutRequestID" => $stkdata->stk->CheckoutRequestID,
                                    "ResponseCode" => $stkdata->stk->ResponseCode,
                                    "ResponseDescription"=>$stkdata->stk->ResponseDescription,
                                    "CustomerMessage" => $stkdata->stk->CustomerMessage,
                                    "session_id" => $session_id
                                ];
                                $this->db->insert('msafiri_api_mpesa', $stk_transaction);
                            } else {
                                $sent_response = $this->end("Thank you for trying msafiri, we hope you will signup next time.", "200");
                                echo $sent_response;
                            }
                            
                            break;
//
                        }
                }
                }
    }


    public function msafiri_api($data)
    {
        $api_server_url = $this->config->item('api_server_url');
        $curl = curl_init();
        $token = $this->TokenManager->getMsafiriToken();
        
        curl_setopt_array($curl, array(
            CURLOPT_URL => $api_server_url."/api/v1/msafiri/travel-data",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPHEADER => array(
                "Authorization:Bearer ".$token,
                "Accept: application/json",
                "Content-Type: application/json"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return $status = 0;
        } else {
            return (Object)json_decode($response);
        }
    }
    public function mpesa_stk($data)
    {
        $api_server_url = $this->config->item('api_server_url');
        $curl = curl_init();
        $token = $this->TokenManager->getMsafiriToken();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $api_server_url."/api/v1/msafiri/stk-push",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPHEADER => array(
                "Authorization:Bearer ".$token,
                "Accept: application/json",
                "Content-Type: application/json"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        // die();
        } else {
            return (Object)json_decode($response);
        }
    }
    // public function showtoken()
    // {
    //     $token = new TokenManager();
    //     echo $token->getMsafiriToken();
    // }
}
