<?php

include('Welcome.php');

class Momsclub extends Welcome
{
    public $name;
    public $email;
    public $insurance_medical_card_number;
    public $member_status;
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('TokenManager');
    }

    public function index()
    {
        echo "Hi hater";
    }

    public function session()
    {
        header("Cache-Control: no-cache, must-revalidate");
        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        header('Content-type:application/json');
        header('Access-Control-Allow-Origin: *');
        $session_id = $this->uri->segment(3);
        $session_state = $this->uri->segment(4);
        $postdata = file_get_contents("php://input");
        $postdata = json_decode($postdata);
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $msisdn = $postdata->msisdn;
        } elseif ($_SERVER['REQUEST_METHOD'] === 'PUT') {
            $msisdn = $this->get_put_msisdn();
        }
        $response = array();
        switch ($session_state) {
            case "start":
                $userMSISDN = $this->get_put_msisdn();
                $userMobileNumber = substr($userMSISDN, 3);
                $this->Ussd_models->add_session($session_id, $msisdn, "2", "", "");
                $sent_response = $this->getmenu("52");
                echo $sent_response;

                break;
            case "response":
                $getsession = $this->Ussd_models->get_session($msisdn, $session_id);
                foreach ($getsession->result() as $sessionresult) {
                    $themenuid = $sessionresult->menu_id;
                    $selection = $this->get_put_text();
                    switch ($themenuid) {
                        case 1:
                            $this->Ussd_models->add_session($session_id, $msisdn, "2", "", "");
                            $sent_response = $this->getmenu("52");
                            echo $sent_response;
                            break;
                        case 2:
                            if ($selection == 1) {
                                $this->Ussd_models->add_session($session_id, $msisdn, "4", "", "");
                                $sent_response = $this->getmenu("53");
                                echo $sent_response;
                                $moms_club_data = array('session_id' => $session_id, 'has_medical_policy'=> 1, 'phone_number' => $msisdn);
                                $result = $this->db->insert('moms_club', $moms_club_data);
                            } else {
                                $this->Ussd_models->add_session($session_id, $msisdn, "3", "", "");
                                $sent_response = $this->getmenu("53");
                                echo $sent_response;
                                $moms_club_data = array('session_id' => $session_id, 'has_medical_policy'=> 0, 'phone_number' => $msisdn);
                                $result = $this->db->insert('moms_club', $moms_club_data);
                            }
                            break;
                        case 3:
                            $this->Ussd_models->add_session($session_id, $msisdn, "5", "", "");
                            $sent_response = $this->getmenu("54");
                            echo $sent_response;
                            $moms_club_data = array('name' => $selection);
                            $this->db->where('session_id', $session_id);
                            $result = $this->db->update('moms_club', $moms_club_data);

                            break;
                        case 4:
                            $this->Ussd_models->add_session($session_id, $msisdn, "6", "", "");
                            $sent_response = $this->getmenu("54");
                            echo $sent_response;
                            $moms_club_data = array('name' => $selection);
                            $this->db->where('session_id', $session_id);
                            $result = $this->db->update('moms_club', $moms_club_data);

                            break;
                        case 5:
                            $screen_text = "Thank for registering for mums club, you will be enrolled once you make a payment of KES 500 to paybil 328105";
                            $sent_response = $this->end($screen_text, "200");
                            echo $sent_response;
                            $moms_club_data = array('email' => $selection);
                            $this->db->where('session_id', $session_id);
                            $result = $this->db->update('moms_club', $moms_club_data);

                            break;
                        case 6:
                            $this->Ussd_models->add_session($session_id, $msisdn, "8", "", "");
                            $sent_response = $this->getmenu("55");
                            echo $sent_response;
                            $moms_club_data = array('email' => $selection);
                            $this->db->where('session_id', $session_id);
                            $result = $this->db->update('moms_club', $moms_club_data);

                            break;
                        case 8:
                            $screen_text = "You are enrolled to the Jubilee Mum's Club";
                            $sent_response = $this->end($screen_text, "200");
                            echo $sent_response;
                            $moms_club_data = array('medical_card_no' => $selection);
                            $this->db->where('session_id', $session_id);
                            $result = $this->db->update('moms_club', $moms_club_data);

                            break;
                        default:
                            
                            break;
                    }
                }

                // no break
            default:
                # code...
                break;
        }
    }
    public function msafiri_api($data)
    {
        $api_server_url = $this->config->item('api_server_ip');
        $curl = curl_init();
        $token = $this->TokenManager->getMsafiriToken();
        
        //Need an end point for moms club payment
        curl_setopt_array($curl, array(
            CURLOPT_URL => "/api/v1/msafiri/travel-data",
            // CURLOPT_URL => "http://10.102.6.197/api/v1/msafiri/travel-data",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPHEADER => array(
                "Authorization:Bearer ".$token,
                "Accept: application/json",
                "Content-Type: application/json"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return $status = 0;
        } else {
            return (Object)json_decode($response);
        }
    }

    public function mpesa_stk($data)
    {
        $curl = curl_init();
        $token = $this->TokenManager->getMsafiriToken();
        //moms club stk endpoint needed
        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://digitalappsuat.jubileekenya.com/api/v1/msafiri/stk-push",
            // CURLOPT_URL => "http://10.102.6.197/api/v1/msafiri/stk-push",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPHEADER => array(
                "Authorization:Bearer ".$token,
                "Accept: application/json",
                "Content-Type: application/json"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return (Object)json_decode($response);
        }
    }
}
