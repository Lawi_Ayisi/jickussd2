<?php

/**
 * Created by PhpStorm.
 * User: lawi
 * Date: 12/10/18
 * Time: 12:04 PM
 */
include("Welcome.php");
class Menucontroller extends Welcome
{
    public $userIdnumber;
    public $numberOfpassengers;
    public $whereFrom;
    public $whereTo;
    public $busCompany;
    public $totalCost;
    public $headers;

    public function __construct()
    {
        parent:: __construct();
        $this->load->database();
        // $this->load->model('Ussd_models');
        // $this->load->library('CI_infobip');
        // $this->load->library('session');
    }

    public function index()
    {
        echo "Hi";
    }

    public function session()
    {
        require_once APPPATH . 'libraries/codeigniter-predis/src/Redis.php';
        $this->redis = new \CI_Predis\Redis(['serverName' => 'localhost']);
        $server_url = $this->config->item('server_url');

        header("Cache-Control: no-cache, must-revalidate");
        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        header('Content-type:application/json');
        header('Access-Control-Allow-Origin: *');
        $session_id = $this->uri->segment(3);
        $session_state = $this->uri->segment(4);
        $postdata = file_get_contents("php://input");
        $postdata = json_decode($postdata);
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $msisdn = $postdata->msisdn;
        } elseif ($_SERVER['REQUEST_METHOD'] === 'PUT') {
            $msisdn = $this->get_put_msisdn();
        }
        $response = array();
        switch ($session_state) {
            case "start":
                $userMSISDN = $this->get_put_msisdn();
                $userMobileNumber = substr($userMSISDN, 3);
                $this->Ussd_models->add_session($session_id, $msisdn, "1", "", "");
                $sent_response = $this->getmenu("51");
                echo $sent_response;
                break;

            case "response":
                $getsession = $this->Ussd_models->get_session($msisdn, $session_id);
                foreach ($getsession->result() as $sessionresult) {
                    $themenuid = $sessionresult->menu_id;
                    $selection = $this->get_put_text();
                    $session_level_json = $this->redis->get($session_id);
                    (Object)$session_level = json_decode($session_level_json);
                    if (isset($session_level)) {
                        if ($session_level->app_route == 1) {
//                            $bodydata = json_encode($postdata);
//                            $sent_response = self::getResponse('https://v2mbima.blue-wave.co.ke/rest/infobip/session/' . $session_id . '/response', $headers, $bodydata);
//                            echo $sent_response;
                            $sent_response = $this->end("Dear customer, Imarisha Jamii is currently undergoing maintainance and is unavailble to process your request.", "200");
                            echo $sent_response;
                        } elseif ($session_level->app_route == 2) {
                            $bodydata = json_encode($postdata);
                            $sent_response = self::getResponse($server_url.'/jickussd2/index.php/welcome/session/' . $session_id . '/response', $headers, $bodydata);
                            echo $sent_response;
                        } elseif ($session_level->app_route == 3) {
                            $bodydata = json_encode($postdata);
                            $sent_response = self::getResponse($server_url.'/jickussd2/index.php/msafiri/session/' . $session_id . '/response', $headers, $bodydata);
                            echo $sent_response;
                        } elseif ($session_level->app_route == 4) {
                            $bodydata = json_encode($postdata);
                            $sent_response = self::getResponse($server_url.'/jickussd2/index.php/momsclub/session/' . $session_id . '/response', $headers, $bodydata);
                            echo $sent_response;
                        } elseif ($session_level->app_route == 5) {
                            $sent_response = $this->end("Please read the Terms and Conditions here: http://bit.ly/2wyI0Dd.", "200");
                            echo $sent_response;
                        } elseif ($session_level->app_route == 6) {
                            $sent_response = $this->end("Contact Jubilee Insurance on: Phone:070991000 or 0719222111. Email:info@jubileekenya.com", "200");
                            echo $sent_response;
                        }
                    } else {
                        switch ($themenuid) {
                        case 1:
                            switch ($selection) {
                                case 1:
                                    $sent_response = $this->end("Dear customer, Imarisha Jamii is currently undergoing maintainance and is unavailble to process your request.", "200");
                                    echo $sent_response;
                                    break;
                                case 2:
                                    $this->Ussd_models->add_session($session_id, $msisdn, "4", "", "");
                                    $bodydata = json_encode($postdata);
                                    $sent_response = self::getResponse($server_url.'/jickussd2/index.php/welcome/session/' . $session_id . '/start', $headers, $bodydata);
                                    echo $sent_response;
                                    $session_data_arr = [
                                        'app_route' => $selection,
                                        'session_level' => 'start'
                                    ];
                                    $session_data = json_encode($session_data_arr);
                                    $this->redis->set($session_id, $session_data);
                                    break;
                                case 3:
                                    $this->Ussd_models->add_session($session_id, $msisdn, "4", "", "");
                                    $bodydata = json_encode($postdata);
                                    $sent_response = self::getResponse($server_url.'/jickussd2/index.php/msafiri/session/' . $session_id . '/start', $headers, $bodydata);
                                    echo $sent_response;
                                    $session_data_arr = [
                                        'app_route' => $selection,
                                        'session_level' => 'start'
                                    ];
                                    $session_data = json_encode($session_data_arr);
                                    $this->redis->set($session_id, $session_data);
                                    break;
                                case 4:
                                    $this->Ussd_models->add_session($session_id, $msisdn, "4", "", "");
                                    $bodydata = json_encode($postdata);
                                    $sent_response = self::getResponse($server_url.'/jickussd2/index.php/momsclub/session/' . $session_id . '/start', $headers, $bodydata);
                                    echo $sent_response;
                                    $session_data_arr = [
                                        'app_route' => $selection,
                                        'session_level' => 'start'
                                    ];
                                    $session_data = json_encode($session_data_arr);
                                    $this->redis->set($session_id, $session_data);
                                    break;
                                case 5:
                                    $sent_response = $this->end("Please read the Terms and Conditions here: http://bit.ly/2wyI0Dd.", "200");
                                    echo $sent_response;
                                    break;
                                case 6:
                                    $sent_response = $this->end("Contact Jubilee Insurance on: Phone:070991000 or 0719222111. Email:info@jubileekenya.com", "200");
                                    echo $sent_response;
                                    break;
                            }
                            //Welcome menu
                            break;
                        case 2:
                            break;
                        case 3:
                             //Welcome menu
                            break;
                        case 4:
                            $this->Ussd_models->add_session($session_id, $msisdn, "5", "", "");
                            $session_level_json = $this->redis->get($session_id);
                            $session_level = json_decode($session_level_json);
                            $bodydata = json_encode($postdata);
                            $sent_response = self::getResponse($server_url.'/jickussd2/index.php/msafiri/session/' . $session_id . '/response', $headers, $bodydata);
                            echo $sent_response;
                            break;
                    }
                    }
                }
            }
    }
    public static function getResponse($url, $headers, $body)
    {
        $params = '?' . http_build_query($headers);

        $redirect_url = $url . $params;

        $ch = curl_init($redirect_url);

        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);

        if (!isset($response)) {
            return null;
        }
        return $response;
    }
}
