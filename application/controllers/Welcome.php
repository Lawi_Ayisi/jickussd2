<?php /** @noinspection ALL */
// Turn off error reporting
error_reporting(0);
defined('BASEPATH') OR exit('No direct script access allowed');

//$lifePolicyNumber="";

class Welcome extends CI_Controller
{

    public $generalAmount;
    public $general_policy_number;
    public $general_customer_policy_number;
    public $policyClass;
    public $policy_data_key_value = array();
    public $policyNumbersAndTypes = array();
    public $customerChoice;
    public $customerAmount;
    public $amountToPay;
    public $policyPayAgainst;
    public $lifePolicyNumber = "";
    public $generalPolicyNumber = "";
    public $medicalCustomerMemberNumber = "";
    public $msisdn = "";
    public $postgres_server_url = "";
    public $graphql_server_url = "";

    public function __construct()
    {
        parent:: __construct();
        $this->load->database();
        $this->load->model('Ussd_models');
        $this->load->library('CI_infobip');
        $this->load->library('session');
        $this->postgres_server_url = $this->config->item('postgres_server_url');
        $this->graphql_server_url = $this->config->item('graphql_server_url');
    }

    public function index()
    {
//        $today = date('d-m-Y');
//        $policyEndDate = date('d-m-Y', strtotime($today));
//        $policyRenewalTime = strtotime(stripslashes('12 May 2018'));
//        $dateOfPolicyRenewal = date("d-m-Y", strtotime(' + 60 days', $policyRenewalTime));
//        echo $dateOfPolicyRenewal . " and today" . $policyEndDate;
        $LifePolicyInfo = $this->postgres_server_url."/customer?phone_no=" . '0713348090';
        $ch = curl_init($LifePolicyInfo);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $policyData = curl_exec($ch);
        curl_close($ch);
        $data_string = json_decode($policyData, true);
        $node = $data_string['customer_details'];
        var_dump($node);
    }


    public function session()
    {
        header("Cache-Control: no-cache, must-revalidate");
        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        header('Content-type:application/json');
        header('Access-Control-Allow-Origin: *');
        $session_id = $this->uri->segment(3);
        $session_state = $this->uri->segment(4);
        $postdata = file_get_contents("php://input");
        $postdata = json_decode($postdata);
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $msisdn = $postdata->msisdn;
        } elseif ($_SERVER['REQUEST_METHOD'] === 'PUT') {
            $msisdn = $this->get_put_msisdn();
        }
        $response = array();
        switch ($session_state) {
            //Registration or Login based on user's phone number.
            case "start":
                $userMSISDN = $this->get_put_msisdn();
                $userMobileNumber = substr($userMSISDN, 3);
                $customerNumber = "0" . $userMobileNumber;
                $LifePolicyInfo = $this->postgres_server_url."/customer?phone_no=" . $customerNumber;
                $ch = curl_init($LifePolicyInfo);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HEADER, 0);
                $policyData = curl_exec($ch);
                curl_close($ch);

                $data_string = json_decode($policyData, true);
                $node = $data_string['customer_details'];
                foreach ($node as $key => $value) {
                    $know_your_client_id = $value['kyc_id'];
                    if ($know_your_client_id == "") {
                        $customer_message = "Dear customer, you have successfully registered with the Jubilee Insurance USSD service. To view your account information, please call our contact centre on 0709949000 and 0719222111 or visit our nearest branch.";
                        $this->ci_infobip->sendsms($customer_message, $userMSISDN);
                    }
                }
                $getuser = $this->Ussd_models->get_user($msisdn);
                $numb = $getuser->num_rows();
                if ($numb > 0) {
                    $this->Ussd_models->add_session($session_id, $msisdn, "24", "", "");
                    $sent_response = $this->getmenu("24");
                } else {
                    $this->Ussd_models->add_session($session_id, $msisdn, "0", "", "");
                    $sent_response = $this->getmenu("1");
                }
                echo $sent_response;
                break;
            //check user response to determine what menu to display
            case "response":
                $getsession = $this->Ussd_models->get_session($msisdn, $session_id);
                foreach ($getsession->result() as $sessionresult) {
                    $themenuid = $sessionresult->menu_id;
                    $selection = $this->get_put_text();
                    switch ($themenuid) {
                        case 0:
                            $add_user = $this->Ussd_models->add_user($msisdn, $selection);
                            $this->Ussd_models->add_session($session_id, $msisdn, "1", "", "");
                            $sent_response = $this->getmenu("2");
                            echo $sent_response;
                            break;
                        case 1:
                            /////////////////////////////////////////////////////
                            //TODO::GET Request to determine if the customer is uniquely identified

                            $userMSISDN = $this->get_put_msisdn();
                            $userMobileNumber = substr($userMSISDN, 3);
                            $customerNumber = "0" . $userMobileNumber;
                            $LifePolicyInfo = $this->postgres_server_url."/customer?phone_no=" . $customerNumber;
                            $ch = curl_init($LifePolicyInfo);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_HEADER, 0);
                            $policyData = curl_exec($ch);
                            curl_close($ch);
                            $data_string = json_decode($policyData, true);
                            $node = $data_string['customer_details'];
                            $_SESSION['customerDetails'] = $node;
                            //TODO::Kitu empty
                            if (empty($node)) {
                                $this->Ussd_models->add_session($session_id, $msisdn, "34", "", "");
                                $customer_message = "Dear customer, you have successfully registered with the Jubilee Insurance USSD service. To view your account information, please call our contact centre on 0709949000 and 0719222111 or visit our nearest branch.";
                                $this->ci_infobip->sendsms($customer_message, $userMSISDN);
                                $this->Ussd_models->add_pwd($msisdn, md5($selection));
                                $sent_response = $this->getmenu("34");
                                echo $sent_response;
                            } else {
                                $this->Ussd_models->add_pwd($msisdn, md5($selection));
                                $system_customer_id = $this->get_system_user_national_id();
                                $user_entered_id = $this->Ussd_models->get_user_national_id($msisdn);
                                if ($user_entered_id == $system_customer_id) {
                                    $this->Ussd_models->add_session($session_id, $msisdn, "3", "", "");
                                    $sent_response = $this->getmenu("3");
                                    echo $sent_response;
                                }
                            }
                            break;
                        case 2:
                            $enterd_user_password = md5($selection);
                            $get_pwd = $this->Ussd_models->get_user_password($msisdn);
                            $user_saved_pawword = $get_pwd[0]['pwd'];
                            $system_customer_id = $this->get_user_national_id();
                            if ($enterd_user_password == $user_saved_pawword) {
                                $sent_response = $this->getmenu("3");
                            } else {
                                $sent_response = $this->end();
                            }
                            break;
                        case 3:
                            if ($selection == 1) {
                                //handle life
                                $this->Ussd_models->add_session($session_id, $msisdn, "11", "", "");
                                $sent_response = $this->getmenuitems("1");
                                echo $sent_response;
                            } elseif ($selection == 2) {
                                //handle pension
                                $this->Ussd_models->add_session($session_id, $msisdn, "12", "", "");
                                $sent_response = $this->getmenuitems("2");
                                echo $sent_response;
                            } elseif ($selection == 3) {
                                //handle medical
                                $this->Ussd_models->add_session($session_id, $msisdn, "13", "", "");
                                $sent_response = $this->getmenuitems("3");
                                echo $sent_response;
                            } elseif ($selection == 4) {
                                //handle general
                                $this->Ussd_models->add_session($session_id, $msisdn, "14", "", "");
                                $sent_response = $this->getmenuitems("4");
                                echo $sent_response;
                            } elseif ($selection == 5) {
                                //handle talk to jubilee
                                $this->Ussd_models->add_session($session_id, $msisdn, "4", "", "");
                                $sent_response = $this->getmenu("4");
                                echo $sent_response;
                            } elseif ($selection == 6) {
                                //handle validate agent menu (6)
                                $this->Ussd_models->add_session($session_id, $msisdn, "6", "", "");
                                $sent_response = $this->getmenu("6");
                                echo $sent_response;
                            }
                            break;
                        case 4:
                            if ($selection == 1) {
                                $this->Ussd_models->add_session($session_id, $msisdn, "3", "", "");
                                $sent_response = $this->getmenu("3");
                                echo $sent_response;
                            } elseif ($selection == 2) {
                                $sent_response = $this->end("Thank you for using our services", "200");
                                echo $sent_response;
                            } else {
                                //save comment
                                $this->Ussd_models->add_comment($msisdn, $selection);
                                //Send mail to customer center
                                $config = Array(
                                    'protocol' => 'smtp',
                                    'smtp_host' => 'ssl://smtp.googlemail.com',
                                    'smtp_port' => 465,
                                    'smtp_user' => 'jubileedeveloper@gmail.com',
                                    'smtp_pass' => 'Jn&-~^yx}r@]cjs',
                                    'mailtype'  => 'html',
                                    'charset'   => 'iso-8859-1'
                                );
                                $this->load->library('email', $config);
                                $this->email->set_newline("\r\n");
                                $this->email->from('noreply@jubileekenya.com', 'USSD Customer Question');
                                $this->email->to('customerservice@jubileekenya.com');
                                $this->email->cc('lawi.ayisi@jubileekenya.com');
                                $this->email->bcc('erick.wasambo@jubileekenya.com');
                                $this->email->subject('USSD Customer Questions');
                                $this->email->message('Customer Number: '.$msisdn."<br>".'Customer Question: '.$selection);
                                $this->email->send();
                                $this->Ussd_models->add_session($session_id, $msisdn, "5", "", "");
                                $sent_response = $this->getmenu("5");
                                echo $sent_response;
                            }
                            break;
                        case 5:
                            if ($selection == 1) {
                                $this->Ussd_models->add_session($session_id, $msisdn, "5", "", "");
                                $sent_response = $this->getmenuitems("2");
                                echo $sent_response;
                            } elseif ($selection == 2) {
                                $sent_response = $this->end("Thank you for using our services", "200");
                                echo $sent_response;
                            }
                            break;
                        case 6:
                            if ($selection == 1) {
                                //Life Agent
                                $this->Ussd_models->add_session($session_id, $msisdn, "7", "", "");
                                $sent_response = $this->getmenu("7");
                                echo $sent_response;
                            } elseif ($selection == 2) {
                                //General Agent
                                $this->Ussd_models->add_session($session_id, $msisdn, "41", "", "");
                                $sent_response = $this->getmenu("41");
                                echo $sent_response;
                            } elseif ($selection == 3) {
                                //Medical Agent
                                $this->Ussd_models->add_session($session_id, $msisdn, "42", "", "");
                                $sent_response = $this->getmenu("42");
                                echo $sent_response;
                            } elseif ($selection == 4) {
                                //Pension Agent
                                $this->Ussd_models->add_session($session_id, $msisdn, "43", "", "");
                                $sent_response = $this->getmenu("43");
                                echo $sent_response;
                            } elseif ($selection == 5) {
                                if (empty($this->session->customerDetails) == false) {
                                    $this->Ussd_models->add_session($session_id, $msisdn, "3", "", "");
                                    $sent_response = $this->getmenu("3");
                                    echo $sent_response;
                                } elseif (empty($this->session->customerDetails) == true) {
                                    $this->Ussd_models->add_session($session_id, $msisdn, "34", "", "");
                                    $sent_response = $this->getmenu("34");
                                    echo $sent_response;
                                }

                            } elseif ($selection == 6) {
                                //Exit
                                $sent_response = $this->end("Thank you for using our services", "200");
                                echo $sent_response;
                            }
                            break;
                        case 7:
                            $this->Ussd_models->add_session($session_id, $msisdn, "65", "", "");
                            $sent_response = $this->lifeAgentSearch();
                            echo $sent_response;
                            break;
                        case 8:
                            if ($selection == 1) {
                                $this->Ussd_models->add_session($session_id, $msisdn, "3", "", "");
                                $sent_response = $this->getmenu("9");
                                echo $sent_response;
                            } elseif ($selection == 2) {
                                $sent_response = $this->end("Thank you for using our services", "200");
                                echo $sent_response;
                            }
                            break;
                        // life payment based on user feedback
                        case 10:
                            /*if ($selection == 1) {
                                $this->Ussd_models->add_session($session_id, $msisdn, "15", "", "");
                                //$sent_response=$this->getmenu("15");
                                $sent_response = $this->getLifeMenuList();
                                echo $sent_response;
                            } else*/ if ($selection == 1) {
                            $customer_phonenumber = $this->get_put_msisdn();
                            //$customer_message = 'Account Name: JICK LIFE & PENSION BUSINESS'."\n".'Beneficiary Name: The Jubilee Insurance Company of Kenya'."\n".'Account Number: 0104126133'."\n".'Bank & Branch Details: Diamond Trust Bank Kenya Limited, Nation Center'."\n".'Bank & Branch Code: 63-001'."\n".'Swift Code: DTKEKENA'."\n".'Currency: USD'."\n".'Address: P.O. Box 61711, 00200 C/SQ, NAIROBI'."\n".'Telephone Number: +2540202849000';
                            $customer_message = 'You may choose to make payment to Jubilee Insurance via Cash or Cheque at any of our branches countrywide. If you wish to do a bank transfer, direct deposit please click on http://jubileeinsure.com/jubileeapks/payment_details_jick.pdf to view our account details.';
                            if ($this->ci_infobip->sendsms($customer_message, $customer_phonenumber)) {
                                $sent_response = $this->end("Thank you for using our services. An SMS with payment details will be sent to your phone number shortly.", "200");
                                echo $sent_response;
                            } else {
                                $sent_response = $this->end("We encountered a problem. Please try again later", "200");
                                echo $sent_response;
                            }
                        } elseif ($selection == 2) {
                            $this->Ussd_models->add_session($session_id, $msisdn, "11", "", "");
                            $sent_response = $this->getmenuitems("1");
                            echo $sent_response;
                        }
                            break;
                        //Life policy cases
                        case 11:
                            if ($selection == 1) {
                                $this->Ussd_models->add_session($session_id, $msisdn, "54", "", "");
                                $sent_response = $this->getLifeMenuList();
                                echo $sent_response;
                            } elseif ($selection == 2) {
                                //premiums
                                $this->Ussd_models->add_session($session_id, $msisdn, "55", "", "");
                                //$sent_response=$this->lifePremiumInfo();
                                $sent_response = $this->getLifeMenuList();
                                echo $sent_response;
                            } elseif ($selection == 3) {
                                //Make payment
                                $this->Ussd_models->add_session($session_id, $msisdn, "10", "", "");
                                $sent_response = $this->getmenu("10");
                                echo $sent_response;
                            } elseif ($selection == 4) {
                                //loan
                                $this->Ussd_models->add_session($session_id, $msisdn, "36", "", "");
                                $sent_response = $this->getmenu("36");
                                echo $sent_response;

                            } elseif ($selection == 5) {
                                //loan
                                $this->Ussd_models->add_session($session_id, $msisdn, "67", "", "");
                                //$sent_response=$this->lifePolicyStatement();
                                $sent_response = $this->getLifeMenuList();
                                echo $sent_response;

                            } elseif ($selection == 6) {
                                $this->Ussd_models->add_session($session_id, $msisdn, "3", "", "");
                                $sent_response = $this->getmenu("3");
                                echo $sent_response;
                            } elseif ($selection == 7) {
                                $sent_response = $this->end("Thank you for using our services", "200");
                                echo $sent_response;
                            }
                            break;
                        //Pension policy cases
                        case 12:
                            if ($selection == 00) {
                                $this->Ussd_models->add_session($session_id, $msisdn, "3", "", "");
                                $sent_response = $this->getmenu("3");
                                echo $sent_response;
                            } elseif ($selection == 1) {
                                //account info
                                if ($selection == 00) {
                                    $this->Ussd_models->add_session($session_id, $msisdn, "3", "", "");
                                    $sent_response = $this->getmenu("3");
                                    echo $sent_response;
                                } else
                                    $this->Ussd_models->add_session($session_id, $msisdn, "12", "", "");
                                $sent_response = $this->pensionPolicyInfo();
                                echo $sent_response;
                            } elseif ($selection == 2) {
                                //balance
                                $this->Ussd_models->add_session($session_id, $msisdn, "62", "", "");
                                $sent_response = $this->pensionBalance();
                                echo $sent_response;
                            } elseif ($selection == 3) {
                                //make performance
                                $this->Ussd_models->add_session($session_id, $msisdn, "62", "", "");
                                $sent_response = $this->pensionPerfomance();
                                echo $sent_response;

                            } elseif ($selection == 4) {
                                //previous payment
                                $this->Ussd_models->add_session($session_id, $msisdn, "38", "", "");
                                $sent_response = $this->getmenu("38");
                                echo $sent_response;

                            } elseif ($selection == 5) {
                                $this->Ussd_models->add_session($session_id, $msisdn, "3", "", "");
                                $sent_response = $this->getmenu("3");
                                echo $sent_response;
                            } elseif ($selection == 6) {
                                $sent_response = $this->end("Thank you for using our services", "200");
                                echo $sent_response;
                            }
                            break;
                        //Medical Cases
                        case 13:
                            if ($selection == 1) {
                                //policy info
                                $this->Ussd_models->add_session($session_id, $msisdn, "46", "", "");
                                $sent_response = $this->getMedicalMenuList();
                                echo $sent_response;

                            } elseif ($selection == 2) {
                                //medical benefits
                                $this->Ussd_models->add_session($session_id, $msisdn, "52", "", "");
                                //$sent_response=$this->medicalPolicyBenefits();
                                $sent_response = $this->getMedicalMenuList();
                                echo $sent_response;

                            } elseif ($selection == 3) {
                                //medical services provider
                                //send text message
                                $customer_phonenumber = $this->get_put_msisdn();
                                //$customer_phonenumber = 254713348090;
                                $customer_message = "Link to medical service providers https://www.jubileeinsurance.com/ke/index.php/customer-relations/medical-providers-list";
                                if ($this->ci_infobip->sendsms($customer_message, $customer_phonenumber)) {
                                    $sent_response = $this->end("Thank you for using our services. SMS with a link to our service providers will be sent to you shortly", "200");
                                    echo $sent_response;
                                } else {
                                    $sent_response = $this->end("We encountered a problem. Please try again later", "200");
                                    echo $sent_response;
                                }
                            } elseif ($selection == 4) {
                                //make payment
                                $this->Ussd_models->add_session($session_id, $msisdn, "37", "", "");
                                $sent_response = $this->getmenu("37");
                                echo $sent_response;

                            } elseif ($selection == 5) {
                                //emergency contacts
                                //send text message
                                $customer_phonenumber = $this->get_put_msisdn();
                                //$customers_name = ucfirst(strtolower($FirstName));
                                $customer_message = "Emergency Medical contact number: 0709949000 \n 0719222111";
                                if ($this->ci_infobip->sendsms($customer_message, $customer_phonenumber)) {
                                    $sent_response = $this->end("Thank you for using our services. SMS with the emergency contacts will be sent to you shortly.", "200");
                                    echo $sent_response;
                                } else {
                                    $sent_response = $this->end("We encountered a problem. Please try again later", "200");
                                    echo $sent_response;
                                }
                            } elseif ($selection == 6) {
                                $this->Ussd_models->add_session($session_id, $msisdn, "3", "", "");
                                $sent_response = $this->getmenu("3");
                                echo $sent_response;
                            } elseif ($selection == 7) {
                                $sent_response = $this->end("Thank you for using our services", "200");
                                echo $sent_response;
                            }
                            break;
                        //General Cases
                        case 14:
                            if ($selection == 1) {
                                //policy info
                                $this->Ussd_models->add_session($session_id, $msisdn, "20", "", "");
                                $sent_response = $this->getGeneralMenuList();
                                echo $sent_response;
                            } elseif ($selection == 2) {
                                //premiums
                                $this->Ussd_models->add_session($session_id, $msisdn, "51", "", "");
                                $sent_response = $this->getGeneralMenuList();
                                echo $sent_response;
                            } elseif ($selection == 3) {
                                //make payment
                                $this->Ussd_models->add_session($session_id, $msisdn, "39", "", "");
                                $sent_response = $this->getmenu("39");
                                echo $sent_response;
                            } elseif ($selection == 4) {
                                $this->Ussd_models->add_session($session_id, $msisdn, "3", "", "");
                                $sent_response = $this->getmenu("3");
                                echo $sent_response;
                            } elseif ($selection == 5) {
                                $sent_response = $this->end("Thank you for using our services", "200");
                                echo $sent_response;
                            }
                            break;
                        case 15:
                            if ($selection == 00) {
                                $this->Ussd_models->add_session($session_id, $msisdn, "11", "", "");
                                $sent_response = $this->getmenuitems("1");
                                echo $sent_response;
                            } else {
                                $this->Ussd_models->add_session($session_id, $msisdn, "68", "", "");
                                // $sent_response = $this->payLifePolicy($selection);
                                $sent_response = $this->getLifeMenuList();
                                echo $sent_response;
                            }
                            break;
                        case 16:
                            //make payment against general policy number
                            $userMSISDN = $this->get_put_msisdn();
                            $userMobileNumber = substr($userMSISDN, 3);
                            $customerNumber = "0" . $userMobileNumber;
                            $LifePolicyInfo = $this->postgres_server_url."/policies?phone_no=" . $customerNumber;
                            $ch = curl_init($LifePolicyInfo);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_HEADER, 0);
                            $policyData = curl_exec($ch);
                            curl_close($ch);
                            $data_string = json_decode($policyData, true);
                            $node = $data_string['policies'];
                            $general_customer_policy_number = "";
                            foreach ($node as $key => $value) {
                                $customer_line_of_business = $value['line_of_business'];
                                if ($customer_line_of_business == "GENERAL") {
                                    $general_customer_policy_number = $value['pol_number'];
                                }
                            }
                            $data = array(
                                "amount" => $selection,
                                "number" => $this->get_put_msisdn(),
                                "policynumber" => $general_customer_policy_number
                            );
                            if ($this->postToPayment("http://192.168.50.47/mpesa_api/requestcheckout", $data) == true) {
                                /*$this->Ussd_models->add_session($session_id, $msisdn, "5", "", "");
                                $sent_response = $this->getmenu("5");*/
                                $sent_response = $this->end("Thank you for using our services", "200");
                                echo $sent_response;
                            }
                            break;
                        case 20:
                            $this->Ussd_models->add_session($session_id, $msisdn, "57", "", "");
                            $sent_response = $this->globalGeneralPolicyDetails($selection);
                            echo $sent_response;
                            break;
                        case 21:
                            if ($selection == 1) {
                                // $selection == $general_policy_number;
                                $this->Ussd_models->add_session($session_id, $msisdn, "22", "", "");
                                $sent_response = $this->getmenu("18");
                                echo $sent_response;
                            } else if ($selection == 99) {
                                $this->Ussd_models->add_session($session_id, $msisdn, "3", "", "");
                                $sent_response = $this->getmenu("3");
                                echo $sent_response;
                            }
                            break;
                        case 22:
                            if ($selection == 99) {
                                $this->Ussd_models->add_session($session_id, $msisdn, "3", "", "");
                                $sent_response = $this->lifeTheftPolicyInfo();
                                echo $sent_response;
                            } else if ($selection == 9) {
                                $this->Ussd_models->add_session($session_id, $msisdn, "21", "", "");
                                $sent_response = $this->lifePackagePolicyInfo();
                                echo $sent_response;
                            } else if ($selection == 10) {
                                $this->Ussd_models->add_session($session_id, $msisdn, "21", "", "");
                                $sent_response = $this->lifeMiscellaneoussPolicyInfo();
                                echo $sent_response;
                            } else if ($selection == 11) {
                                $this->Ussd_models->add_session($session_id, $msisdn, "21", "", "");
                                $sent_response = $this->getmenu("21");
                                echo $sent_response;
                            }
                            break;
                        case 23:
                            if ($selection == 99) {
                                $this->Ussd_models->add_session($session_id, $msisdn, "3", "", "");
                                $sent_response = $this->getmenu("3");
                                echo $sent_response;
                            } else if ($selection == 9) {
                                $this->Ussd_models->add_session($session_id, $msisdn, "21", "", "");
                                $sent_response = $this->getmenu("21");
                                echo $sent_response;
                            } else if ($selection == 10) {
                                $this->Ussd_models->add_session($session_id, $msisdn, "21", "", "");
                                $sent_response = $this->getmenu("21");
                                echo $sent_response;
                            } else if ($selection == 11) {
                                $this->Ussd_models->add_session($session_id, $msisdn, "21", "", "");
                                $sent_response = $this->getmenu("21");
                                echo $sent_response;
                            }
                            break;
                        case 24:
                            $entered_user_password = md5($selection);
                            $get_pwd = $this->Ussd_models->get_user_password($msisdn);
                            $system_customer_id = $this->get_system_user_national_id();
                            $user_entered_id = $this->Ussd_models->get_user_national_id($msisdn);
                            if ($entered_user_password == $get_pwd && $system_customer_id == $user_entered_id) {
                                $this->Ussd_models->add_session($session_id, $msisdn, "3", "", "");
                                $sent_response = $this->getmenu("3");
                                echo $sent_response;
                            } else if ($entered_user_password != $get_pwd && $system_customer_id == $user_entered_id) {
                                $this->Ussd_models->add_session($session_id, $msisdn, "25", "", "");
                                $sent_response = $this->getmenu("25");
                                echo $sent_response;
                            } else if ($entered_user_password == $get_pwd && $system_customer_id != $user_entered_id) {
                                $this->Ussd_models->add_session($session_id, $msisdn, "34", "", "");
                                $sent_response = $this->getmenu("34");
                                echo $sent_response;
                            } else if ($entered_user_password != $get_pwd && $system_customer_id != $user_entered_id) {
                                $this->Ussd_models->add_session($session_id, $msisdn, "25", "", "");
                                $sent_response = $this->getmenu("25");
                                echo $sent_response;
                            } else {
                                $sent_response = $this->end("Thank you for using our services", "200");
                                echo $sent_response;
                            }
                            break;
                        case 25:
                            if ($selection == 1) {
                                $this->Ussd_models->add_session($session_id, $msisdn, "24", "", "");
                                $sent_response = $this->getmenu("24");
                                echo $sent_response;
                            } else if ($selection == 2) {
                                $this->Ussd_models->add_session($session_id, $msisdn, "72", "", "");
                                $sent_response = $this->getmenu("45");
                                echo $sent_response;
                            } else if ($selection == 3) {
                                $sent_response = $this->end("Thank you for using our services", "200");
                                echo $sent_response;
                            }
                            break;
                        case 26:
                            $this->Ussd_models->add_session($session_id, $msisdn, "27", "", "");
                            $sent_response = $this->emergency_contact_number();
                            echo $sent_response;
                            break;
                        case 27:
                            //make payment against pensions policy number
                            $userMSISDN = $this->get_put_msisdn();
                            $userMobileNumber = substr($userMSISDN, 3);
                            $customerNumber = "0" . $userMobileNumber;
                            $LifePolicyInfo = $this->postgres_server_url."/policies?phone_no=" . $customerNumber;
                            $ch = curl_init($LifePolicyInfo);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_HEADER, 0);
                            $policyData = curl_exec($ch);
                            curl_close($ch);
                            $data_string = json_decode($policyData, true);
                            $node = $data_string['policies'];
                            $pension_customer_policy_number = "";
                            foreach ($node as $key => $value) {
                                $customer_line_of_business = $value['line_of_business'];
                                if ($customer_line_of_business == "PENSIONS") {
                                    $pension_customer_policy_number = $value['pol_number'];
                                }
                            }
                            $data = array(
                                "amount" => $selection,
                                "number" => $this->get_put_msisdn(),
                                "policynumber" => $pension_customer_policy_number
                            );
                            if ($this->postToPayment("http://192.168.50.47/mpesa_api/requestcheckout", $data) == true) {
                                $this->Ussd_models->add_session($session_id, $msisdn, "5", "", "");
                                $sent_response = $this->getmenu("5");
                                echo $sent_response;
                            }
                            break;
                        case 28:
                            //make payment against medical policy number
                            $userMSISDN = $this->get_put_msisdn();
                            $userMobileNumber = substr($userMSISDN, 3);
                            $customerNumber = "0" . $userMobileNumber;
                            $LifePolicyInfo = $this->postgres_server_url."/policies?phone_no=" . $customerNumber;
                            $ch = curl_init($LifePolicyInfo);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_HEADER, 0);
                            $policyData = curl_exec($ch);
                            curl_close($ch);
                            $data_string = json_decode($policyData, true);
                            $node = $data_string['policies'];
                            $medical_customer_policy_number = "";
                            foreach ($node as $key => $value) {
                                $customer_line_of_business = $value['line_of_business'];
                                if ($customer_line_of_business == "MEDICAL") {
                                    $medical_customer_policy_number = $value['pol_number'];
                                }
                            }
                            $data = array(
                                "amount" => $selection,
                                "number" => $this->get_put_msisdn(),
                                "policynumber" => $medical_customer_policy_number
                            );
                            if ($this->postToPayment("http://192.168.50.47/mpesa_api/requestcheckout", $data) == true) {
                                $this->Ussd_models->add_session($session_id, $msisdn, "5", "", "");
                                $sent_response = $this->getmenu("5");
                                echo $sent_response;
                            }
                            break;
                        // pension payment based on user feedback
                        case 29:
                            if ($selection == 1) {
                                $this->Ussd_models->add_session($session_id, $msisdn, "27", "", "");
                                $sent_response = $this->getmenu("27");
                                echo $sent_response;
                            } elseif ($selection == 2) {
                                $this->Ussd_models->add_session($session_id, $msisdn, "38", "", "");
                                $sent_response = $this->getmenu("38");
                                echo $sent_response;
                            }
                            break;
                        // general payment based on user feedback
                        case 30:
                            if ($selection == 1) {
                                $this->Ussd_models->add_session($session_id, $msisdn, "31", "", "");
                                $sent_response = $this->getmenu("31");
                                echo $sent_response;
                            } elseif ($selection == 2) {
                                $this->Ussd_models->add_session($session_id, $msisdn, "3", "", "");
                                $sent_response = $this->getmenu("3");
                                echo $sent_response;
                            }
                            break;
                        case 31:
                            //make payment against medical policy number
                            $userMSISDN = $this->get_put_msisdn();
                            $userMobileNumber = substr($userMSISDN, 3);
                            $customerNumber = "0" . $userMobileNumber;
                            $LifePolicyInfo = $this->postgres_server_url."/policies?phone_no=" . $customerNumber;
                            $ch = curl_init($LifePolicyInfo);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_HEADER, 0);
                            $policyData = curl_exec($ch);
                            curl_close($ch);
                            $data_string = json_decode($policyData, true);
                            $node = $data_string['policies'];
                            $medical_customer_policy_number = "";
                            foreach ($node as $key => $value) {
                                $customer_line_of_business = $value['line_of_business'];
                                if ($customer_line_of_business == "GENERAL") {
                                    $general_customer_policy_number = $value['pol_number'];
                                }
                            }
                            $data = array(
                                "amount" => $selection,
                                "number" => $this->get_put_msisdn(),
                                "policynumber" => $general_customer_policy_number
                            );
                            if ($this->postToPayment("http://192.168.50.47/mpesa_api/requestcheckout", $data) == true) {
                                $this->Ussd_models->add_session($session_id, $msisdn, "5", "", "");
                                $sent_response = $this->getmenu("5");
                                echo $sent_response;
                            }
                            break;
                        case 34:
                            if ($selection == 1) {
                                //Ask jubilee
                                $this->Ussd_models->add_session($session_id, $msisdn, "35", "", "");
                                $sent_response = $this->getmenu("35");
                                echo $sent_response;
                            } else if ($selection == 2) {
                                //handle validate agent menu (6)
                                $this->Ussd_models->add_session($session_id, $msisdn, "6", "", "");
                                $sent_response = $this->getmenu("6");
                                echo $sent_response;
                            } else if ($selection == 3) {
                                //handle validate agent menu (6)
                                $sent_response = $this->end("Thank you for using our services", "200");
                                echo $sent_response;
                            }
                            break;
                        case 35:
                            if ($selection == 1) {
                                $this->Ussd_models->add_session($session_id, $msisdn, "34", "", "");
                                $sent_response = $this->getmenu("34");
                                echo $sent_response;
                            } elseif ($selection == 2) {
                                $sent_response = $this->end("Thank you for using our services", "200");
                                echo $sent_response;
                            } else {
                                //save comment
                                $this->Ussd_models->add_comment($msisdn, $selection);
                                $this->Ussd_models->add_session($session_id, $msisdn, "34", "", "");
                                $sent_response = $this->getmenu("5");
                                echo $sent_response;
                            }
                            break;
                        case 36:
                            if ($selection == 1) {
                                $this->Ussd_models->add_session($session_id, $msisdn, "66", "", "");
                                //$sent_response=$this->lifeLoanLimit();
                                $sent_response = $this->getLifeMenuList();
                                echo $sent_response;
                            } elseif ($selection == 2) {
                                $customer_phonenumber = $this->get_put_msisdn();
                                //$customer_message = 'Account Name: JICK LIFE & PENSION BUSINESS'."\n".'Beneficiary Name: The Jubilee Insurance Company of Kenya'."\n".'Account Number: 0104126133'."\n".'Bank & Branch Details: Diamond Trust Bank Kenya Limited, Nation Center'."\n".'Bank & Branch Code: 63-001'."\n".'Swift Code: DTKEKENA'."\n".'Currency: USD'."\n".'Address: P.O. Box 61711, 00200 C/SQ, NAIROBI'."\n".'Telephone Number: +2540202849000';
                                $customer_message = 'To apply for a loan, please submit your National ID, KRA PIN certificate, Policy Document and a duly filled Loan Bond Form at your nearest branch. Please click on https://jubileeinsure.com/jubileeapks/Loan_Bond.pdf to download the form.';
                                if ($this->ci_infobip->sendsms($customer_message, $customer_phonenumber)) {
                                    //$this->Ussd_models->add_session($session_id, $msisdn, "61", "", "");
                                    $message = "Thank you for using our services. An SMS with loan requirements will be sent to your phone number shortly. \n\n Reply: \n 00. Back \n 11. Exit";
                                    $sent_response = $this->ussd_core(false, $message, 200, "");
                                    echo $sent_response;
                                } else {
                                    $sent_response = $this->end("We encountered a problem. Please try again later", "200");
                                    echo $sent_response;
                                }
                            } elseif ($selection == 3) {
                                $this->Ussd_models->add_session($session_id, $msisdn, "11", "", "");
                                $sent_response = $this->getmenuitems("1");
                                echo $sent_response;
                            } elseif ($selection == 4) {
                                $sent_response = $this->end("Thank you for using our services", "200");
                                echo $sent_response;
                            }
                            break;
                        case 37:
                            /*if ($selection == 1) {
                                $this->Ussd_models->add_session($session_id, $msisdn, "70", "", "");
                                $sent_response = $this->getMedicalMenuList();
                                //$sent_response=$this->getMedicalPayingMenuList();
                                echo $sent_response;
                            } else*/ if ($selection == 1) {
                            $customer_phonenumber = $this->get_put_msisdn();
                            //$customer_message = 'Account Name: JICK LIFE & PENSION BUSINESS'."\n".'Beneficiary Name: The Jubilee Insurance Company of Kenya'."\n".'Account Number: 0104126133'."\n".'Bank & Branch Details: Diamond Trust Bank Kenya Limited, Nation Center'."\n".'Bank & Branch Code: 63-001'."\n".'Swift Code: DTKEKENA'."\n".'Currency: USD'."\n".'Address: P.O. Box 61711, 00200 C/SQ, NAIROBI'."\n".'Telephone Number: +2540202849000';
                            $customer_message = 'You may choose to make payment to Jubilee Insurance via Cash or Cheque at any of our branches countrywide. If you wish to do a bank transfer, direct deposit please click on https://jubileeinsure.com/paymentdetails/payment_details_JICK.pdf to view our account details.';
                            if ($this->ci_infobip->sendsms($customer_message, $customer_phonenumber)) {
                                $sent_response = $this->end("Thank you for using our services. An SMS with payment details will be sent to your phone number shortly.", "200");
                                echo $sent_response;
                            } else {
                                $sent_response = $this->end("We encountered a problem. Please try again later", "200");
                                echo $sent_response;
                            }
                        } elseif ($selection == 2) {
                            $this->Ussd_models->add_session($session_id, $msisdn, "3", "", "");
                            $sent_response = $this->getmenu("3");
                            echo $sent_response;
                        }
                            break;
                        case 38:
                            /*if ($selection == 1) {
                                $this->Ussd_models->add_session($session_id, $msisdn, "29", "", "");
                                $sent_response = $this->getmenu("29");
                                echo $sent_response;
                            } else*/ if ($selection == 1) {
                            $customer_phonenumber = $this->get_put_msisdn();
                            //$customer_message = 'Account Name: JICK LIFE & PENSION BUSINESS'."\n".'Beneficiary Name: The Jubilee Insurance Company of Kenya'."\n".'Account Number: 0104126133'."\n".'Bank & Branch Details: Diamond Trust Bank Kenya Limited, Nation Center'."\n".'Bank & Branch Code: 63-001'."\n".'Swift Code: DTKEKENA'."\n".'Currency: USD'."\n".'Address: P.O. Box 61711, 00200 C/SQ, NAIROBI'."\n".'Telephone Number: +2540202849000';
                            $customer_message = 'You may choose to make payment to Jubilee Insurance via Cash or Cheque at any of our branches countrywide. If you wish to do a bank transfer, direct deposit please click on https://jubileeinsure.com/paymentdetails/payment_details_JICK.pdf to view our account details.';
                            if ($this->ci_infobip->sendsms($customer_message, $customer_phonenumber)) {
                                $this->Ussd_models->add_session($session_id, $msisdn, "62", "", "");
                                $message = "Thank you for using our services. An SMS with the loan requirements will be sent to you shortly. \n\n Reply: \n 00. Back \n 11. Exit";
                                $sent_response = $this->ussd_core(false, $message, 200, "");
                                echo $sent_response;
                            } else {
                                $sent_response = $this->end("We encountered a problem. Please try again later", "200");
                                echo $sent_response;
                            }
                        } elseif ($selection == 2) {
                            $this->Ussd_models->add_session($session_id, $msisdn, "12", "", "");
                            $sent_response = $this->getmenuitems("2");
                            echo $sent_response;
                        } elseif ($selection == 3) {
                            $sent_response = $this->end("Thank you for using our services", "200");
                            echo $sent_response;
                        }
                            break;
                        case 39:
                            /*if ($selection == 1) {
                                $this->Ussd_models->add_session($session_id, $msisdn, "48", "", "");
                                //$sent_response=$this->getmenu("31");
                                $sent_response = $this->getGeneralMenuList();
                                echo $sent_response;
                            } else */if ($selection == 1) {
                            $customer_phonenumber = $this->get_put_msisdn();
                            //$customer_message = 'Account Name: JICK LIFE & PENSION BUSINESS'."\n".'Beneficiary Name: The Jubilee Insurance Company of Kenya'."\n".'Account Number: 0104126133'."\n".'Bank & Branch Details: Diamond Trust Bank Kenya Limited, Nation Center'."\n".'Bank & Branch Code: 63-001'."\n".'Swift Code: DTKEKENA'."\n".'Currency: USD'."\n".'Address: P.O. Box 61711, 00200 C/SQ, NAIROBI'."\n".'Telephone Number: +2540202849000';
                            $customer_message = 'You may choose to make payment to Jubilee Insurance via Cash or Cheque at any of our branches countrywide. If you wish to do a bank transfer, direct deposit please click on https://jubileeinsure.com/paymentdetails/payment_details_JICK.pdf to view our account details.';
                            if ($this->ci_infobip->sendsms($customer_message, $customer_phonenumber)) {
                                $sent_response = $this->end("Thank you for using our services. An SMS with payment details will be sent to your phone number shortly.", "200");
                                echo $sent_response;
                            } else {
                                $sent_response = $this->end("We encountered a problem. Please try again later", "200");
                                echo $sent_response;
                            }
                        } elseif ($selection == 2) {
                            $this->Ussd_models->add_session($session_id, $msisdn, "3", "", "");
                            $sent_response = $this->getmenu("3");
                            echo $sent_response;
                        }
                            break;
                        case 40:
                            if ($this->Ussd_models->reset_pwd($msisdn, md5($selection))) {
                                $message = "Your new password is " . $selection;
                                $customer_phonenumber = $this->get_put_msisdn();
                                if ($this->ci_infobip->sendsms($message, $customer_phonenumber)) {
                                    $this->Ussd_models->add_session($session_id, $msisdn, "24", "", "");
                                    $sent_response = $this->getmenu("24");
                                    echo $sent_response;
                                } else {
                                    $sent_response = $this->end("We encountered a problem. Please try again later", "200");
                                    echo $sent_response;
                                }
                            }
                            break;
                        case 41:
                            if ($selection == 00) {
                                if (empty($this->session->customerDetails) == false) {
                                    $this->Ussd_models->add_session($session_id, $msisdn, "3", "", "");
                                    $sent_response = $this->getmenu("3");
                                    echo $sent_response;
                                } elseif (empty($this->session->customerDetails) == true) {
                                    $this->Ussd_models->add_session($session_id, $msisdn, "41", "", "");
                                    $sent_response = $this->getmenu("34");
                                    echo $sent_response;
                                }
                            } elseif ($selection == 11) {
                                $sent_response = $this->end("Thank you for using our services", "200");
                                echo $sent_response;
                            } else {
                                $this->Ussd_models->add_session($session_id, $msisdn, "41", "", "");
                                $sent_response = $this->lifeAgentSearch();
                                echo $sent_response;
                            }
                            break;
                        case 42:
                            if ($selection == 00) {
                                if (empty($this->session->customerDetails) == false) {
                                    $this->Ussd_models->add_session($session_id, $msisdn, "3", "", "");
                                    $sent_response = $this->getmenu("3");
                                    echo $sent_response;
                                } elseif (empty($this->session->customerDetails) == true) {
                                    $this->Ussd_models->add_session($session_id, $msisdn, "42", "", "");
                                    $sent_response = $this->getmenu("34");
                                    echo $sent_response;
                                }
                            } else {
                                $this->Ussd_models->add_session($session_id, $msisdn, "42", "", "");
                                $sent_response = $this->medicalAgentSearch($selection);
                                echo $sent_response;
                            }
                            break;
                        case 43:
                            if ($selection == 00) {
                                if (empty($this->session->customerDetails) == false) {
                                    $this->Ussd_models->add_session($session_id, $msisdn, "3", "", "");
                                    $sent_response = $this->getmenu("3");
                                    echo $sent_response;
                                } elseif (empty($this->session->customerDetails) == true) {
                                    $this->Ussd_models->add_session($session_id, $msisdn, "42", "", "");
                                    $sent_response = $this->getmenu("34");
                                    echo $sent_response;
                                }
                            } else {
                                $this->Ussd_models->add_session($session_id, $msisdn, "42", "", "");
                                $sent_response = $this->medicalAgentSearch($selection);
                                echo $sent_response;
                            }
                            break;
                        case 44:
                            $this->Ussd_models->add_session($session_id, $msisdn, "44", "", "");
                            $sent_response = $this->pensionAgentSearch($selection);
                            echo $sent_response;
                            break;
                        case 45:
                            $this->Ussd_models->add_session($session_id, $msisdn, "44", "", "");
                            $sent_response = $this->lifeFireIndividualPolicyInfo();
                            echo $sent_response;
                            break;
                        case 46:
                            if ($selection == 00) {
                                $this->Ussd_models->add_session($session_id, $msisdn, "13", "", "");
                                $sent_response = $this->getmenuitems("3");
                                echo $sent_response;
                            } else {
                                $this->Ussd_models->add_session($session_id, $msisdn, "46", "", "");
                                $sent_response = $this->individualMedicalPolicyInformation($selection);
                                echo $sent_response;
                            }
                            break;
                        case 47:
                            if ($selection == 1) {
                                $this->Ussd_models->add_session($session_id, $msisdn, "47", "", "");
                                $sent_response = $this->payMedicalWithPolicyNumber();
                                echo $sent_response;
                            }
                            break;
                        case 48:
                            $this->Ussd_models->add_session($session_id, $msisdn, "49", "", "");
                            $sent_response = $this->payGeneralPolicy($selection);
                            echo $sent_response;
                            break;
                        case 49:
                            $this->Ussd_models->add_session($session_id, $msisdn, "49", "", "");
                            $sent_response = $this->generalPolicyCheckoutRequest($selection);
                            echo $sent_response;
                            break;
                        case 50:
                            if ($selection == 1) {
                                $this->Ussd_models->add_session($session_id, $msisdn, "11", "", "");
                                $sent_response = $this->getmenu("11");
                                echo $sent_response;
                            } elseif ($selection == 2) {
                                $sent_response = $this->end("Thank you for using our services", "200");
                                echo $sent_response;
                            }
                            break;
                        case 51:
                            if ($selection == 00) {
                                $this->Ussd_models->add_session($session_id, $msisdn, "14", "", "");
                                $sent_response = $this->getmenuitems("4");
                                echo $sent_response;
                            } elseif ($selection == 11) {
                                $sent_response = $this->end("Thank you for using our services", "200");
                                echo $sent_response;
                            } else {
                                $this->Ussd_models->add_session($session_id, $msisdn, "51", "", "");
                                $sent_response = $this->globalGeneralPremiumDetails($selection);
                                echo $sent_response;
                            }
                            break;
                        case 52:
                            if ($selection == 00) {
                                $this->Ussd_models->add_session($session_id, $msisdn, "13", "", "");
                                $sent_response = $this->getmenuitems("3");
                                echo $sent_response;
                            } else {
                                $this->Ussd_models->add_session($session_id, $msisdn, "52", "", "");
                                $sent_response = $this->individualMedicalPolicyBenefitsInformation($selection);
                                echo $sent_response;
                            }
                            break;
                        case 53:
                            $this->Ussd_models->add_session($session_id, $msisdn, "56", "", "");
                            $message = "";
                            /*$individualAccidentPolicyNumbers= array();
                            $this->policy_data_key_value = $individualAccidentPolicyNumbers;*/
                            $individualAccidentPolicyNumbers = array(
                                '1' => 'apple',
                                '2' => 'orange',
                                '3' => 'grape',
                                '4' => 'apple',
                                '5' => 'apple');
                            foreach ($individualAccidentPolicyNumbers as $key => $value) {
                                $message = $message . $key . ". " . $value . "\n";
                            }
                            $sent_response = $this->ussd_core(false, $message, 200, "");
                            echo $sent_response;
                            break;
                        case 54:
                            if ($selection == 00) {
                                $this->Ussd_models->add_session($session_id, $msisdn, "11", "", "");
                                $sent_response = $this->getmenuitems("1");
                                echo $sent_response;
                            } elseif ($selection == 11) {
                                $sent_response = $this->end("Thank you for using our services", "200");
                                echo $sent_response;
                            } else {
                                $this->Ussd_model->add_session($session_id, $msisdn, "61", "", "");
                                $sent_response = $this->globalLifePolicyNumbers($selection);
                                echo $sent_response;
                            }
                            break;
                        case 55:
                            if ($selection == 00) {
                                $this->Ussd_model->add_session($session_id, $msisdn, "11", "", "");
                                $sent_response = $this->getmenuitems("1");
                                echo $sent_response;
                            } elseif ($selection == 11) {
                                $sent_response = $this->end("Thank you for using our services", "200");
                                echo $sent_response;
                            } else {
                                $this->Ussd_model->add_session($session_id, $msisdn, "61", "", "");
                                $sent_response = $this->lifePremiumInfo($selection);
                                echo $sent_response;
                            }
                            break;
                        case 57:
                            if ($selection == 00) {
                                $this->Ussd_models->add_session($session_id, $msisdn, "14", "", "");
                                $sent_response = $this->getmenuitems("4");
                                echo $sent_response;
                            } elseif ($selection == 11) {
                                $sent_response = $this->end("Thank you for using our services", "200");
                                echo $sent_response;
                            } else {
                                $this->Ussd_models->add_session($session_id, $msisdn, "57", "", "");
                                $sent_response = $this->globalGeneralPolicyDetails($selection);
                                echo $sent_response;
                            }
                            break;
                        case 58:
                            $this->Ussd_models->add_session($session_id, $msisdn, "59", "", "");
//                            $pol_number = $this->globalPolicyNumbers($selection);
//                            $this->payGeneralWithPolicyNumber($pol_number);
                            $sent_response = $this->getmenu("31");
//                            $sent_response = $this->ussd_core(false, $message,200,"");
                            echo $sent_response;
                            break;
                        case 59:
                            $this->Ussd_models->add_session($session_id, $msisdn, "60", "", "");
                            $this->customerAmount = $selection;
                            $sent_response = $this->amountToPay = $this->customerAmount;
//                            $sent_response = $this->ussd_core(false, $message,200,"");
                            echo $sent_response;
                            break;
                        case 60:
                            $this->Ussd_models->add_session($session_id, $msisdn, "60", "", "");
                            $this->customerAmount = $selection;
                            $pol_number = $this->globalPolicyNumbers($selection);
                            $this->payGeneralWithPolicyNumber($pol_number, $this->customerAmount);
                            $sent_response = $this->ussd_core(false, $message, 200, "");
                            echo $sent_response;
                            break;
                        case 61:
                            if ($selection == 00) {
                                $this->Ussd_models->add_session($session_id, $msisdn, "11", "", "");
                                $sent_response = $this->getmenuitems("1");
                                echo $sent_response;
                            } elseif ($selection == 11) {
                                $sent_response = $this->end("Thank you for using our services", "200");
                                echo $sent_response;
                            } else {
                                $this->Ussd_models->add_session($session_id, $msisdn, "61", "", "");
                                $sent_response = $this->globalLifePolicyNumbers($selection);
                                echo $sent_response;
                            }
                            break;
                        case 62:
                            if ($selection == 00) {
                                $this->Ussd_models->add_session($session_id, $msisdn, "12", "", "");
                                $sent_response = $this->getmenuitems("2");
                                echo $sent_response;
                            } elseif ($selection == 11) {
                                $sent_response = $this->end("Thank you for using our services", "200");
                                echo $sent_response;
                            }
                            break;
                        case 63:
                            if ($selection == 00) {
                                $this->Ussd_models->add_session($session_id, $msisdn, "14", "", "");
                                $sent_response = $this->getmenuitems("4");
                                echo $sent_response;
                            } elseif ($selection == 11) {
                                $sent_response = $this->end("Thank you for using our services", "200");
                                echo $sent_response;
                            }
                            break;
                        case 64:
                            if ($selection == 00) {
                                $this->Ussd_models->add_session($session_id, $msisdn, "3", "", "");
                                $sent_response = $this->getmenu("3");
                                echo $sent_response;
                            } elseif ($selection == 11) {
                                $sent_response = $this->end("Thank you for using our services", "200");
                                echo $sent_response;
                            }
                            break;
                        case 65:
                            if ($selection == 00) {
                                if (empty($this->session->customerDetails) == false) {
                                    $this->Ussd_models->add_session($session_id, $msisdn, "3", "", "");
                                    $sent_response = $this->getmenu("3");
                                    echo $sent_response;
                                } elseif (empty($this->session->customerDetails) == true) {
                                    $this->Ussd_models->add_session($session_id, $msisdn, "34", "", "");
                                    $sent_response = $this->getmenu("34");
                                    echo $sent_response;
                                }

                            } elseif ($selection == 11) {
                                $sent_response = $this->end("Thank you for using our services", "200");
                                echo $sent_response;
                            }
                            break;
                        case 66:
                            if ($selection == 00) {
                                $this->Ussd_models->add_session($session_id, $msisdn, "3", "", "");
                                $sent_response = $this->getmenu("3");
                                echo $sent_response;
                            } elseif ($selection == 11) {
                                $sent_response = $this->end("Thank you for using our services", "200");
                                echo $sent_response;
                            } else {
                                $this->Ussd_models->add_session($session_id, $msisdn, "66", "", "");
                                $sent_response = $this->lifeLoanLimit($selection);
                                echo $sent_response;
                            }
                            break;
                        case 67:
                            if ($selection == 00) {
                                $this->Ussd_models->add_session($session_id, $msisdn, "11", "", "");
                                $sent_response = $this->getmenuitems("1");
                                echo $sent_response;
                            } elseif ($selection == 11) {
                                $sent_response = $this->end("Thank you for using our services", "200");
                                echo $sent_response;
                            } else {
                                $this->Ussd_models->add_session($session_id, $msisdn, "67", "", "");
                                $sent_response = $this->globalLifeMiniStatement($selection);
                                echo $sent_response;
                            }
                            break;
                        case 68:
                            $this->Ussd_models->add_session($session_id, $msisdn, "69", "", "");
                            $sent_response = $this->payLifePolicy($selection);
                            echo $sent_response;
                            break;
                        case 69:
                            $this->Ussd_models->add_session($session_id, $msisdn, "69", "", "");
                            $sent_response = $this->lifePolicyCheckoutRequest($selection);
                            echo $sent_response;
                            break;
                        case 70:
                            $this->Ussd_models->add_session($session_id, $msisdn, "71", "", "");
                            $sent_response = $this->payMedicalPolicy($selection);
                            echo $sent_response;
                            break;
                        case 71:
                            $this->Ussd_models->add_session($session_id, $msisdn, "71", "", "");
                            $sent_response = $this->medicalPolicyCheckoutRequest($selection);
                            echo $sent_response;
                            break;
                        case 72:
                            $system_customer_id = $this->get_system_user_national_id();
                            $user_entered_national_id = $selection;
                            $get_saved_user_national_id = $this->Ussd_models->get_user_national_id($msisdn);
                            if($system_customer_id != null){
                                if ($user_entered_national_id == $system_customer_id) {
                                    $this->Ussd_models->add_session($session_id, $msisdn, "40", "", "");
                                    $sent_response = $this->getmenu("40");
                                    echo $sent_response;
                                } else {
                                    $sent_response = $this->end("The National ID number you have entered does not match our records. Kindly call our contact centre on 0709949000 or 0719222111 for assistance.", "200");
                                    echo $sent_response;
                                }
                            } elseif($system_customer_id == null){
                                if($get_saved_user_national_id != null && $user_entered_national_id == $get_saved_user_national_id){
                                    $this->Ussd_models->add_session($session_id, $msisdn, "40", "", "");
                                    $sent_response = $this->getmenu("40");
                                    echo $sent_response;
                                }else {
                                    $sent_response = $this->end("The National ID number you have entered does not match our records. Kindly call our contact centre on 0709949000 or 0719222111 for assistance.", "200");
                                    echo $sent_response;
                                }
                            }

                            break;
                        default:
                            $sent_response = $this->end("We encountered a problem. Please try again later", "200");
                            echo $sent_response;
                            break;
                    }
                }
                break;
            case "end":
                $message = "Thank you for using our services";
                $sent_response = $this->theexit($message);
                echo $sent_response;
                break;
            default:
                $message = "We experienced an error. Please try again later.";
                $sent_response = $this->ussd_core(true, $message, 200, "");
                $sent_response = json_encode($response);
                echo $sent_response;
        }

    }

    function log($filename, $text)
    {
        $file = fopen($filename, "w+");
        fwrite($file, $text);
        fclose($file);
    }

    function get_put_msisdn()
    {

        $data = file_get_contents("php://input");
        $putdata = json_decode($data);
        $msisdn = $putdata->msisdn;
        return $msisdn;
    }

    function ussd_core($shouldclose, $ussdmenu, $responseexitcode, $responseMessage)
    {
        $response["shouldClose"] = $shouldclose;
        $response["ussdMenu"] = $ussdmenu;
        $response["responseExitCode"] = $responseexitcode;
        $response["responseMessage"] = $responseMessage;
        $sent_response = json_encode($response);
        return $sent_response;
    }

    function getmenu($menuid)
    {
        $getmenu = $this->Ussd_models->get_menu($menuid);
        foreach ($getmenu->result() as $menuresult) {
            $themenu = $menuresult->menu;
            //$table_row[] = $eventname;
        }
        $sent_response = $this->ussd_core(false, $themenu, 200, "");
        return $sent_response;
    }

    function getmenuitems($policyid)
    {

        $all_menus = "";
        $getmenuitems = $this->Ussd_models->get_menu_items($policyid);
        $x = $getmenuitems->num_rows() + 1;
        foreach ($getmenuitems->result() as $menuitemresults) {
            $x--;
            $themenuitem = $menuitemresults->meu_name;
            $all_menus = $x . ". " . $themenuitem . "\n" . $all_menus;
            //$table_row[] = $eventname;
        }
        $sent_response = $this->ussd_core(false, $all_menus, 200, "");
        return $sent_response;
    }

    function menu_list()
    {
        $message = "Hello, Welcome to Jubilee Insurance. Please choose:\n1. Existing customer\n2. New customer\n3. Exit";
        $sent_response = $this->ussd_core(false, $message, 200, "");
        return $sent_response;
    }

    function products_list()
    {
        $message = "Please choose\n1. Life insurance services\n2. Pension services\n3. Medical insurance services\n4. General insurance services\n5. Talk to jubilee\n6. Validate agent";
        $sent_response = $this->ussd_core(false, $message, 200, "");
        return $sent_response;
    }

    function end($msg, $code)
    {
        $message = $msg;
        $sent_response = $this->ussd_core(true, $message, $code, "");
        return $sent_response;

    }

    function theexit($msg)
    {
        $message = $msg;
        $sent_response = $this->ussd_core(true, $message, 600, "");
        return $sent_response;

    }

    function get_put_text()
    {
        $data = file_get_contents("php://input");
        $putdata = json_decode($data);
        $text = $putdata->text;
        return $text;
    }

    function get_system_user_national_id()
    {
        $userMSISDN = $this->get_put_msisdn();
        $userMobileNumber = substr($userMSISDN, 3);
        $customerNumber = "0" . $userMobileNumber;
        $LifePolicyInfo = $this->postgres_server_url."/customer?phone_no=" . $customerNumber;
        //$LifePolicyInfo = "http://192.168.52.47:2300/customer?phone_no=0723450405";
        $ch = curl_init($LifePolicyInfo); // such as http://example.com/example.xml
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $policyData = curl_exec($ch);
        curl_close($ch);

        $data_string = json_decode($policyData, true);
        $node = $data_string['customer_details'];
        $customer_id_number = "";
        foreach ($node as $key => $value) {
            $customer_id_number = $value['national_id_no'];
        }
        return $customer_id_number;
    }

    public function globalLifePolicyNumbers($selection)
    {
        $policyNumbersAndTypes = array();
        $userMSISDN = $this->get_put_msisdn();
        $userMobileNumber = substr($userMSISDN, 3);
        $customerNumber = "0" . $userMobileNumber;
        $LifePolicyInfo = $this->postgres_server_url."/policies?phone_no=" . $customerNumber;
        //$LifePolicyInfo = "http://192.168.52.47:2300/policies?phone_no=0723139922";
        $ch = curl_init($LifePolicyInfo); // such as http://example.com/example.xml
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $policyData = curl_exec($ch);
        curl_close($ch);
        $data_string = json_decode($policyData, true);

        if (empty($data_string)) {
            $sent_response = $this->end("We encountered a problem. Please try again later", "200");
            echo $sent_response;
        }
        $node = $data_string['policies'];
        foreach ($node as $key => $value) {
            $customer_line_of_business = $value['line_of_business'];
            if ($customer_line_of_business == "LIFE") {
                $policyNumber = $value['pol_number'];
                $policyNo = '$policyNo';
                $data = array(
                    "query" => "query ($policyNo: String!){policyDetails (policyNo: $policyNo) { edges { node   {  policyNo premiumAmount premiumDueDate premiumFrequency totalPremiumPaid nextOutDate issueDate startDate sumAssured termInYears bonus agentName surrenderValue maturityDate policyType loanLimit loanEligible status } }}}",
                    "variables" => "{\"policyNo\":\"$policyNumber\"}"
                );
                $data_string = json_encode($data);
                $curl = curl_init($this->graphql_server_url.'/isf_graphql'); // server
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                        'Content-Type: application/json',
                        'Content-Length: ' . strlen($data_string))
                );
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  // Make it so the data coming back is put into a string
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);  // Insert the data
                // Send the request
                $result = curl_exec($curl);
                // Free up the resources $curl is using
                curl_close($curl);
                $result = json_decode($result, true);
                $node = $result['data']['policyDetails']['edges'][0];
                foreach ($node as $key => $value) {
                    $policyType = $value['policyType'];
                    $policyNumbersAndTypes[$policyNumber] = $policyType;
                }
            }
        }
        $policy_count = 0;
        $life_customer_policy_number = "";
        foreach ($policyNumbersAndTypes as $policyNumber => $policyType) {
            $policy_count += 1;
            if ($selection == $policy_count) {
                $life_customer_policy_number = $policyNumber;
            }
        }
        $policyNo = '$policyNo';
        $data = array(
            "query" => "query ($policyNo: String!){policyDetails (policyNo: $policyNo) { edges { node   { osPremium policyNo premiumAmount premiumDueDate premiumFrequency totalPremiumPaid nextOutDate issueDate startDate sumAssured termInYears bonus agentName surrenderValue maturityDate policyType loanLimit loanEligible status } }}}",
            "variables" => "{\"policyNo\":\"$life_customer_policy_number\"}"
        );
        $data_string = json_encode($data);
        $curl = curl_init($this->graphql_server_url.'/isf_graphql'); // server
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  // Make it so the data coming back is put into a string
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);  // Insert the data
        // Send the request
        $result = curl_exec($curl);
        // Free up the resources $curl is using
        curl_close($curl);
        $result = json_decode($result, true);
        $node = $result['data']['policyDetails']['edges'][0];
        $life_customer_startDate = "";
        $life_customer_policyType = "";
        $life_customer_sumAssured = "";
        $life_customer_termInYears = "";
        $life_customer_bonus = "";
        $life_customer_surrenderValue = "";
        $life_customer_maturityDate = "";
        $life_customer_agentName = "";
        $life_customer_policy_issueDate = "";
        $life_customer_premiumDue = "";
        $customer_policy_status = "";
        foreach ($node as $key => $value) {
            $life_customer_startDate = $value['startDate'];
            $life_customer_sumAssured = $value['sumAssured'];
            $life_customer_termInYears = $value['termInYears'];
            $life_customer_bonus = $value['bonus'];
            $life_customer_agentName = $value['agentName'];
            $life_customer_surrenderValue = $value['surrenderValue'];
            $life_customer_maturityDate = $value['maturityDate'];
            $life_customer_policyType = $value['policyType'];
            $customer_policy_status = $value['status'];
            $life_customer_policy_issueDate = $value['issueDate'];
            $life_customer_premiumDue = $value['osPremium'];
        }
        //Change date format from datetime to date
        $start_date = strtotime($life_customer_startDate);
        $life_customer_policy_startDate = date('d-m-Y', $start_date);
        $maturity_date = strtotime($life_customer_maturityDate);
        $life_customer_maturity_date = date('d-m-Y', $maturity_date);
        $issue_date = strtotime($life_customer_policy_issueDate);
        $life_customer_issue_date = date('d-m-Y', $issue_date);
        //Format currency by adding commas
        $sum_assured = $life_customer_sumAssured;
        setlocale(LC_MONETARY, "en_US");
        //echo money_format($sum_assured);
        if ($life_customer_premiumDue == null) {
            $life_customer_premiumDue = 0;
        }
        //$message = "Policy Number: " . $life_customer_policy_number. "\nStart Date: " . $life_customer_policy_startDate . "\nIssue Date: " . $life_customer_issue_date . "\nType: " . ucwords(strtolower($life_customer_policyType)) . "\nSum Assured: " . number_format(doubleval($life_customer_sumAssured)) ."\nTerm In Years: " . $life_customer_termInYears . "\nStatus: " . ucwords(strtolower($customer_policy_status)) . "\nAccumulated Bonus: " . number_format($life_customer_bonus) . "\nMaturity Date: " . $life_customer_maturity_date . "\n Agent Name: " . ucwords(strtolower($life_customer_agentName)) . "\nSurrender Value: " . number_format($life_customer_surrenderValue) . "\n\n Reply with\n 00. Back \n 11: Exit";
        $message = "Policy Number: " . $life_customer_policy_number . "\nStart Date: " . $life_customer_policy_startDate . "\nIssue Date: " . $life_customer_issue_date . "\n\n More Details will be sent as SMS \n\n Reply with\n 00. Back";
        $customerMessage = "Policy Number: " . $life_customer_policy_number . "\nStart Date: " . $life_customer_policy_startDate . "\nIssue Date: " . $life_customer_issue_date . "\nType: " . ucwords(strtolower($life_customer_policyType)) . "\nSum Assured: " . number_format(doubleval($life_customer_sumAssured)) . "\nTerm In Years: " . $life_customer_termInYears . "\nStatus: " . ucwords(strtolower($customer_policy_status)) . "\nAccumulated Bonus: " . number_format($life_customer_bonus) . "\nMaturity Date: " . $life_customer_maturity_date . "\n Agent Name: " . ucwords(strtolower($life_customer_agentName)) . "\nSurrender Value: " . number_format($life_customer_surrenderValue);
        if ($this->ci_infobip->sendsms($customerMessage, $this->get_put_msisdn())) {
            $sent_response = $this->ussd_core(false, $customerMessage, 200, "");
            return $sent_response;
        }
    }

    public function lifePremiumInfo($selection)
    {
        $policyNumbersAndTypes = array();
        $userMSISDN = $this->get_put_msisdn();
        $userMobileNumber = substr($userMSISDN, 3);
        $customerNumber = "0" . $userMobileNumber;
        $LifePolicyInfo = $this->postgres_server_url."/policies?phone_no=" . $customerNumber;
        //$LifePolicyInfo = "http://192.168.52.47:2300/premiums?phone_no=0711908640";
        $ch = curl_init($LifePolicyInfo);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $policyData = curl_exec($ch);
        curl_close($ch);
        $data_string = json_decode($policyData, true);

        if (empty($data_string)) {
            $sent_response = $this->end("We encountered a problem. Please try again later", "200");
            echo $sent_response;
        }
        $node = $data_string['policies'];
        foreach ($node as $key => $value) {
            $customer_line_of_business = $value['line_of_business'];
            if ($customer_line_of_business == "LIFE") {
                $policyNumber = $value['pol_number'];
                $policyNo = '$policyNo';
                $data = array(
                    "query" => "query ($policyNo: String!){policyDetails (policyNo: $policyNo) { edges { node   {  policyNo premiumAmount premiumDueDate premiumFrequency totalPremiumPaid nextOutDate issueDate startDate sumAssured termInYears bonus agentName surrenderValue maturityDate policyType loanLimit loanEligible status } }}}",
                    "variables" => "{\"policyNo\":\"$policyNumber\"}"
                );
                $data_string = json_encode($data);
                $curl = curl_init($this->graphql_server_url.'/isf_graphql'); // server
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                        'Content-Type: application/json',
                        'Content-Length: ' . strlen($data_string))
                );
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  // Make it so the data coming back is put into a string
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);  // Insert the data
                // Send the request
                $result = curl_exec($curl);
                // Free up the resources $curl is using
                curl_close($curl);
                $result = json_decode($result, true);
                $node = $result['data']['policyDetails']['edges'][0];
                foreach ($node as $key => $value) {
                    $policyType = $value['policyType'];
                    $policyNumbersAndTypes[$policyNumber] = $policyType;
                }
            }
        }
        $policy_count = 0;
        $life_customer_policy_number = "";
        foreach ($policyNumbersAndTypes as $policyNumber => $policyType) {
            $policy_count += 1;
            if ($selection == $policy_count) {
                $life_customer_policy_number = $policyNumber;
            }
        }

        $policyNo = '$policyNo';
        $data = array(
            "query" => "query ($policyNo: String!){policyDetails (policyNo: $policyNo) { edges { node   { osPremium policyNo premiumAmount premiumDueDate premiumFrequency totalPremiumPaid nextOutDate issueDate startDate sumAssured termInYears bonus agentName surrenderValue maturityDate policyType loanLimit loanEligible status } }}}",
            "variables" => "{\"policyNo\":\"$life_customer_policy_number\"}"
        );

        $data_string = json_encode($data);
        $curl = curl_init($this->graphql_server_url.'/isf_graphql'); // server
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  // Make it so the data coming back is put into a string
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);  // Insert the data
        // Send the request
        $result = curl_exec($curl);
        // Free up the resources $curl is using
        curl_close($curl);
        $result = json_decode($result, true);
        $node = $result['data']['policyDetails']['edges'][0];
        $life_customer_premium_due = "";
        $life_customer_premium_amount = "";
        $life_customer_premium_due_date = "";
        $life_customer_premium_frequency = "";
        $life_customer_totalPremiumPaid = "";
        $life_customer_nextOutDate = "";
        foreach ($node as $key => $value) {
            $life_customer_policynumber = $value['policyNo'];
            $life_customer_premium_amount = $value['premiumAmount'];
            $life_customer_premium_due_date = $value['premiumDueDate'];
            $life_customer_premium_frequency = $value['premiumFrequency'];
            $life_customer_totalPremiumPaid = $value['totalPremiumPaid'];
            $life_customer_nextOutDate = $value['nextOutDate'];
            $life_customer_premium_due = $value['osPremium'];
            $life_customer_issueDate = $value['issueDate'];
        }
        if ($life_customer_premium_due == null) {
            $life_customer_premium_due = 0;
        }
        //Change date format from datetime to date
        $due_date = strtotime($life_customer_premium_due_date);
        $customer_premium_due_date = date('d-m-Y', $due_date);
        $nextOutDate = strtotime($life_customer_nextOutDate);
        $customer_nextOutDate = date('d-m-Y', $nextOutDate);

        $message = "Policy Number: " . $life_customer_policy_number . "\nInstallment: " . number_format($life_customer_premium_amount) . "\nTotal Premium Paid: " . number_format($life_customer_totalPremiumPaid) . "\n Premium Due: " . number_format($life_customer_premium_due) . "\nNext Due Date: " . $customer_nextOutDate . "\n\n Reply with\n 00. Back";
        $sent_response = $this->ussd_core(false, $message, 200, "");
        return $sent_response;
    }

    public function lifeLoanLimit($selection)
    {
        $policyNumbersAndTypes = array();
        $userMSISDN = $this->get_put_msisdn();
        $userMobileNumber = substr($userMSISDN, 3);
        $customerNumber = "0" . $userMobileNumber;
        $LifePolicyInfo = $this->postgres_server_url."/policies?phone_no=" . $customerNumber;
        //$LifePolicyInfo = "http://192.168.52.47:2300/policies?phone_no=0723139922";
        $ch = curl_init($LifePolicyInfo); // such as http://example.com/example.xml
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $policyData = curl_exec($ch);
        curl_close($ch);
        $data_string = json_decode($policyData, true);

        if (empty($data_string)) {
            $sent_response = $this->end("We encountered a problem. Please try again later", "200");
            echo $sent_response;
        }
        $node = $data_string['policies'];
        foreach ($node as $key => $value) {
            $customer_line_of_business = $value['line_of_business'];
            if ($customer_line_of_business == "LIFE") {
                $policyNumber = $value['pol_number'];
                $policyNo = '$policyNo';
                $data = array(
                    "query" => "query ($policyNo: String!){policyDetails (policyNo: $policyNo) { edges { node   {  policyNo premiumAmount premiumDueDate premiumFrequency totalPremiumPaid nextOutDate issueDate startDate sumAssured termInYears bonus agentName surrenderValue maturityDate policyType loanLimit loanEligible status } }}}",
                    "variables" => "{\"policyNo\":\"$policyNumber\"}"
                );
                $data_string = json_encode($data);
                $curl = curl_init($this->graphql_server_url.'/isf_graphql'); // server
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                        'Content-Type: application/json',
                        'Content-Length: ' . strlen($data_string))
                );
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  // Make it so the data coming back is put into a string
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);  // Insert the data
                // Send the request
                $result = curl_exec($curl);
                // Free up the resources $curl is using
                curl_close($curl);
                $result = json_decode($result, true);
                $node = $result['data']['policyDetails']['edges'][0];
                foreach ($node as $key => $value) {
                    $policyType = $value['policyType'];
                    $policyNumbersAndTypes[$policyNumber] = $policyType;
                }
            }
        }

        $policy_count = 0;
        $life_customer_policy_number = "";
        foreach ($policyNumbersAndTypes as $policyNumber => $policyType) {
            $policy_count += 1;
            if ($selection == $policy_count) {
                $life_customer_policy_number = $policyNumber;
            }
        }

        $policyNo = '$policyNo';
        $data = array(
            "query" => "query ($policyNo: String!){policyDetails (policyNo: $policyNo) { edges { node   { osPremium policyNo premiumAmount premiumDueDate premiumFrequency totalPremiumPaid nextOutDate issueDate startDate sumAssured termInYears bonus agentName surrenderValue maturityDate policyType loanLimit loanEligible status } }}}",
            "variables" => "{\"policyNo\":\"$life_customer_policy_number\"}"
        );

        $data_string = json_encode($data);
        $curl = curl_init($this->graphql_server_url.'/isf_graphql'); // server
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  // Make it so the data coming back is put into a string
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);  // Insert the data
        // Send the request
        $result = curl_exec($curl);
        // Free up the resources $curl is using
        curl_close($curl);
        $result = json_decode($result, true);
        $node = $result['data']['policyDetails']['edges'][0];
        $life_customer_loan_elegibility = "";
        $life_customer_loan_limit = "";
        $life_customer_policynumber = "";

        $loan_eligibility = "";
        foreach ($node as $key => $value) {
            $life_customer_policynumber = $value['policyNo'];
            $life_customer_loan_elegibility = $value['loanEligible'];
            $life_customer_loan_limit = $value['loanLimit'];
        }
        if ($life_customer_loan_elegibility == "Y") {
            $loan_eligibility = "Yes";
        } else if ($life_customer_loan_elegibility == "N") {
            $loan_eligibility = "No";
        }
        $message = "Policy Number: " . $life_customer_policynumber . "\nLoan Eligibility: " . $loan_eligibility . "\nGross Loan Limit: " . number_format($life_customer_loan_limit) . "\n\n Reply with\n 00. Back";
        $sent_response = $this->ussd_core(false, $message, 200, "");
        return $sent_response;
    }

    public function globalLifeMiniStatement($selection)
    {
        $policyNumbersAndTypes = array();
        $userMSISDN = $this->get_put_msisdn();
        $userMobileNumber = substr($userMSISDN, 3);
        $customerNumber = "0" . $userMobileNumber;
        $LifePolicyInfo = $this->postgres_server_url."/policies?phone_no=" . $customerNumber;
        //$LifePolicyInfo = "http://192.168.52.47:2300/policies?phone_no=0723139922";
        $ch = curl_init($LifePolicyInfo); // such as http://example.com/example.xml
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $policyData = curl_exec($ch);
        curl_close($ch);
        $data_string = json_decode($policyData, true);

        if (empty($data_string)) {
            $sent_response = $this->end("We encountered a problem. Please try again later", "200");
            echo $sent_response;
        }
        $node = $data_string['policies'];
        foreach ($node as $key => $value) {
            $customer_line_of_business = $value['line_of_business'];
            if ($customer_line_of_business == "LIFE") {
                $policyNumber = $value['pol_number'];
                $policyNo = '$policyNo';
                $data = array(
                    "query" => "query ($policyNo: String!){premiumStatements (policyNo: $policyNo) { edges { node   { policyType policyNo date amount receiptNo type } }}}",
                    "variables" => "{\"policyNo\":\"$policyNumber\"}"
                );
                $data_string = json_encode($data);
                $curl = curl_init($this->graphql_server_url.'/isf_graphql'); // server
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                        'Content-Type: application/json',
                        'Content-Length: ' . strlen($data_string))
                );
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  // Make it so the data coming back is put into a string
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);  // Insert the data
                // Send the request
                $result = curl_exec($curl);
                // Free up the resources $curl is using
                curl_close($curl);
                $result = json_decode($result, true);
                $node2 = $result['data']['premiumStatements']['edges'];
                if(!$node2) {
                    $message = "Your policy is expecting the first premium. Please call our Contact Centre on 0709949000 or 0719222111\n\n Reply with: \n 00. Go Back \n 11. Exit";
                    $sent_response = $this->ussd_core(false, $message, 200, "");
                    return $sent_response;
                }
                else{
                    $node2 = $result['data']['premiumStatements']['edges'][0];
                    foreach ($node2 as $key2 => $value2) {
                        $policyType = $value2['policyType'];
                        $policyNumbersAndTypes[$policyNumber] = $policyType;
                    }
                }
            }
        }

        $policy_count = 0;
        $life_customer_policy_number = "";
        foreach ($policyNumbersAndTypes as $policyNumber => $policyType) {
            $policy_count += 1;
            if ($selection == $policy_count) {
                $life_customer_policy_number = $policyNumber;
            }
        }

        $policyNo = '$policyNo';
        $data = array(
            "query" => "query ($policyNo: String!){premiumStatements (policyNo: $policyNo) { edges { node   {  policyType policyNo date amount receiptNo type } }}}",
            "variables" => "{\"policyNo\":\"$life_customer_policy_number\"}"
        );

        $data_string = json_encode($data);
        $curl = curl_init($this->graphql_server_url.'/isf_graphql'); // server
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  // Make it so the data coming back is put into a string
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);  // Insert the data
        // Send the request
        $result = curl_exec($curl);
        // Free up the resources $curl is using
        curl_close($curl);
        $result = json_decode($result, true);
        $node = $result['data']['premiumStatements']['edges'];
        $message = "";
        $customerMessage = "";
        $customer_phonenumber = $this->get_put_msisdn();
        $policy_count = 0;
        foreach ($node as $key => $value) {
            $policy_count += 1;

            if ($policy_count > 8)
                continue;
            $life_customer_policy_number = $value['node']['policyNo'];
            $life_customer_Receipt_Date = $value['node']['date'];
            $life_customer_paid_amount = $value['node']['amount'];
            $life_customer_receipt_number = $value['node']['receiptNo'];
            $life_customer_policy_type = $value['node']['type'];
            $reciptDate = strtotime($life_customer_Receipt_Date);
            $life_customer_Receipt_Date = date('d-m-Y', $reciptDate);
            $customerMessage = $customerMessage . $policy_count . ". Policy Number: " . $life_customer_policy_number . "\nReceipt Date: " . $life_customer_Receipt_Date . "\nAmount: " . number_format($life_customer_paid_amount) . "\nReceipt Number: " . $life_customer_receipt_number . "\nReceipt Type: " . $life_customer_policy_type . "\n";
            if ($policy_count == 1)
                $message = $message . $policy_count . ". Policy Number: " . $life_customer_policy_number . "\nReceipt Date: " . $life_customer_Receipt_Date . "\nAmount: " . number_format($life_customer_paid_amount) . "\nReceipt Number: " . $life_customer_receipt_number . "\n\n More Details will be sent as SMS";
        }
        if ($this->ci_infobip->sendsms($customerMessage, $customer_phonenumber)) {
            $sent_response = $this->ussd_core(false, $message, 200, "");
            return $sent_response;
        } else {
            $sent_response = $this->end("We encountered a problem. Please try again later", "200");
            echo $sent_response;
        }

    }

    public function globalGeneralPolicyDetails($selection)
    {
        $policyNumbersAndTypes = array();
        $userMSISDN = $this->get_put_msisdn();
        $userMobileNumber = substr($userMSISDN, 3);
        $customerNumber = "0" . $userMobileNumber;
        $LifePolicyInfo = $this->postgres_server_url."/policies?phone_no=" . $customerNumber;
        //$LifePolicyInfo = "http://192.168.52.47:2300/policies?phone_no=0723139922";
        $ch = curl_init($LifePolicyInfo); // such as http://example.com/example.xml
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $policyData = curl_exec($ch);
        curl_close($ch);
        $data_string = json_decode($policyData, true);

        if (empty($data_string)) {
            $sent_response = $this->end("We encountered a problem. Please try again later", "200");
            echo $sent_response;
        }
        $node = $data_string['policies'];
        foreach ($node as $key => $value) {
            $customer_line_of_business = $value['line_of_business'];
            if ($customer_line_of_business == "GENERAL") {
                $policyNumber = $value['pol_number'];
                $policyNo = '$policyNo';
                $data = array(
                    "query" => "query ($policyNo: String!){policyDetails (policyNo: $policyNo) { edges { node   {  fromDate toDate policyNo polClass polSubclass status assuredId } }}}",
                    "variables" => "{\"policyNo\":\"$policyNumber\"}"
                );
                $data_string = json_encode($data);
                $curl = curl_init($this->graphql_server_url.'/premia_graphql'); // server
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                        'Content-Type: application/json',
                        'Content-Length: ' . strlen($data_string))
                );
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  // Make it so the data coming back is put into a string
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);  // Insert the data
                // Send the request
                $result = curl_exec($curl);
                // Free up the resources $curl is using
                curl_close($curl);
                $result = json_decode($result, true);
                $node = $result['data']['policyDetails']['edges'][0];
                foreach ($node as $key => $value) {
                    $policyType = $value['polSubclass'];
                    $policyNumbersAndTypes[$policyNumber] = $policyType;
                }
            }
        }
        $policy_count = 0;
        $life_customer_policy_number = "";
        foreach ($policyNumbersAndTypes as $policyNumber => $policyType) {
            $policy_count += 1;
            if ($selection == $policy_count) {
                $life_customer_policy_number = $policyNumber;
            }
        }
        $policyNo = '$policyNo';
        $data = array(
            "query" => "query ($policyNo: String!){policyDetails (policyNo: $policyNo) { edges { node   {  policyNo polClass polSubclass netPremium toDate status fromDate } }}}",
            "variables" => "{\"policyNo\":\"$policyNumber\"}"
        );
        $data_string = json_encode($data);
        $curl = curl_init($this->graphql_server_url.'/premia_graphql'); // server
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  // Make it so the data coming back is put into a string
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);  // Insert the data
        // Send the request
        $result = curl_exec($curl);
        // Free up the resources $curl is using
        curl_close($curl);
        $result = json_decode($result, true);
        $node = $result['data']['policyDetails']['edges'][0];
        $general_customer_startDate = "";
        $general_customer_endDate = "";
        $general_customer_subclass = "";
        $general_customer_policy_status = "";
        $general_customer_policy_number = "";
        foreach ($node as $key => $value) {
            $general_customer_startDate = $value['fromDate'];
            $general_customer_endDate = $value['toDate'];
            $general_customer_subclass = $value['polSubclass'];
            if ($general_customer_subclass == "PRIVATE CAR - SCHEME POLICY" || $general_customer_subclass == "PRIVATE CAR - BINDER POLICY") {
                $general_customer_subclass = "PRIVATE CAR";
            } elseif ($general_customer_subclass == "MOTOR GENERAL CARTAGE BINDER") {
                $general_customer_subclass = "MOTOR GENERAL CARTAGE";
            } elseif ($general_customer_subclass == "MOTOR CYCLES-BINDER") {
                $general_customer_subclass = "MOTOR CYCLES";
            } elseif ($general_customer_subclass == "MOTOR COMMERCIAL - SCHEME POLI" || $general_customer_subclass == "MOTOR COMMERCIAL - BINDER") {
                $general_customer_subclass = "MOTOR COMMERCIAL";
            } elseif ($general_customer_subclass == "LADY JUBILEE - SCHEME POLICY") {
                $general_customer_subclass = "LADY JUBILEE";
            } elseif ($general_customer_subclass == "TRAVEL INSURANCE-BINDER") {
                $general_customer_subclass = "TRAVEL INSURANCE";
            } elseif ($general_customer_subclass == "MARINE CARGO-BINDER") {
                $general_customer_subclass = "MARINE CARGO";
            } elseif ($general_customer_subclass == "DOMESTIC PACKAGE-BINDER") {
                $general_customer_subclass = "DOMESTIC PACKAGE";
            } elseif ($general_customer_subclass == "GROUP PERSONAL ACCIDENT BINDER") {
                $general_customer_subclass = "GROUP PERSONAL ACCIDENT";
            }
            //$general_customer_policy_status = $value['status'];
            $general_customer_policy_number = $value['policyNo'];
        }
        if (strtotime($general_customer_endDate) > strtotime("today")) {
            $general_customer_policy_status = "Active";
        } elseif (strtotime($general_customer_endDate) < strtotime("today")) {
            $general_customer_policy_status = "Expired";
        }
        //Change date format from datetime to date
        $start_date = strtotime($general_customer_startDate);
        $general_customer_startDate = date('d-m-Y', $start_date);
        $end_date = strtotime($general_customer_endDate);
        $general_customer_endDate = date('d-m-Y', $end_date);
        $message = "Policy Number: " . $general_customer_policy_number . "\nStart Date: " . $general_customer_startDate . "\nEnd Date: " . $general_customer_endDate . "\nStatus: " . $general_customer_policy_status . "\nSub Class: " . ucwords(strtolower($general_customer_subclass)) . "\n\n Reply with\n 00. Back";
        $sent_response = $this->ussd_core(false, $message, 200, "");
        return $sent_response;
    }

    public function globalGeneralPremiumDetails($selection)
    {
        $policyNumbersAndTypes = array();
        $userMSISDN = $this->get_put_msisdn();
        $userMobileNumber = substr($userMSISDN, 3);
        $customerNumber = "0" . $userMobileNumber;
        $LifePolicyInfo = $this->postgres_server_url."/policies?phone_no=" . $customerNumber;
        //$LifePolicyInfo = "http://192.168.52.47:2300/policies?phone_no=0723139922";
        $ch = curl_init($LifePolicyInfo); // such as http://example.com/example.xml
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $policyData = curl_exec($ch);
        curl_close($ch);
        $data_string = json_decode($policyData, true);

        if (empty($data_string)) {
            $sent_response = $this->end("We encountered a problem. Please try again later", "200");
            echo $sent_response;
        }
        $node = $data_string['policies'];
        foreach ($node as $key => $value) {
            $customer_line_of_business = $value['line_of_business'];
            if ($customer_line_of_business == "GENERAL") {
                $policyNumber = $value['pol_number'];
                $policyNo = '$policyNo';
                $data = array(
                    "query" => "query ($policyNo: String!){policyDetails (policyNo: $policyNo) { edges { node   {  fromDate toDate policyNo polClass polSubclass status assuredId } }}}",
                    "variables" => "{\"policyNo\":\"$policyNumber\"}"
                );
                $data_string = json_encode($data);
                $curl = curl_init($this->graphql_server_url.'/premia_graphql'); // server
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                        'Content-Type: application/json',
                        'Content-Length: ' . strlen($data_string))
                );
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  // Make it so the data coming back is put into a string
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);  // Insert the data
                // Send the request
                $result = curl_exec($curl);
                // Free up the resources $curl is using
                curl_close($curl);
                $result = json_decode($result, true);
                $node = $result['data']['policyDetails']['edges'][0];
                foreach ($node as $key => $value) {
                    $policyType = $value['polSubclass'];
                    $policyNumbersAndTypes[$policyNumber] = $policyType;
                }
            }
        }
        $policy_count = 0;
        $life_customer_policy_number = "";
        foreach ($policyNumbersAndTypes as $policyNumber => $policyType) {
            $policy_count += 1;
            if ($selection == $policy_count) {
                $life_customer_policy_number = $policyNumber;
            }
        }
        $policyNo = '$policyNo';
        $data = array(
            "query" => "query ($policyNo: String!){policyDetails (policyNo: $policyNo) { edges { node   {  policyNo polClass polSubclass netPremium toDate status fromDate } }}}",
            "variables" => "{\"policyNo\":\"$policyNumber\"}"
        );
        $data_string = json_encode($data);
        $curl = curl_init($this->graphql_server_url.'/premia_graphql'); // server
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  // Make it so the data coming back is put into a string
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);  // Insert the data
        // Send the request
        $result = curl_exec($curl);
        // Free up the resources $curl is using
        curl_close($curl);
        $result = json_decode($result, true);
        $node = $result['data']['policyDetails']['edges'][0];
        $general_customer_policy_premium = "";
        $general_customer_policy_number = "";
        $general_customer_expiry_date = "";
        foreach ($node as $key => $value) {
            $general_customer_policy_premium = $value['netPremium'];
            $general_customer_policy_number = $value['policyNo'];
            $general_customer_expiry_date = $value['toDate'];
        }
        $expiry_date = strtotime($general_customer_expiry_date);
        $general_customer_expiry_date = date('d-m-Y', $expiry_date);
        if ($general_customer_policy_premium == null) {
            $general_customer_policy_premium = 0;
        }
        $message = "Policy Number: " . $general_customer_policy_number . "\nPremium: " . number_format(doubleval($general_customer_policy_premium)) . "\nExpiry Date: " . $general_customer_expiry_date . "\n\n Reply with\n 00. Back";
        $sent_response = $this->ussd_core(false, $message, 200, "");
        return $sent_response;
    }

    function postToPayment($url, $data)
    {
        $fields = '';
        foreach ($data as $key => $value) {
            $fields .= $key . '=' . $value . '&';
        }
        rtrim($fields, '&');

        $post = curl_init();

        curl_setopt($post, CURLOPT_URL, $url);

        curl_setopt($post, CURLOPT_HEADER, false);
        curl_setopt($post, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($post, CURLOPT_HTTPHEADER,
            array("Content-type: application/x-www-form-urlencoded"));
        curl_setopt($post, CURLOPT_POST, count($data));
        curl_setopt($post, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($post, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($post);

        curl_close($post);
//        var_dump($result);
        return true;
    }

    public function pensionPolicyInfo()
    {
        $userMSISDN = $this->get_put_msisdn();
        $userMobileNumber = substr($userMSISDN, 3);
        $customerNumber = "0" . $userMobileNumber;
        $PensionPolicyInfo = $this->postgres_server_url."/customer?phone_no=" . $customerNumber;
        $ch = curl_init($PensionPolicyInfo);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $policyData = curl_exec($ch);
        curl_close($ch);
        $data_string = json_decode($policyData, true);
        $node = $data_string['customer_details'];
        $pension_customer_member_number = "";
        foreach ($node as $key => $value) {
            $pension_customer_member_number = $value['apex_id'];
        }
        if($pension_customer_member_number == ""){
            $message = "Sorry, you don't have an existing Pension policy with Jubilee Insurance. Please call our Contact Centre on 0709949000 or 0719222111\n\n Reply with: \n 00. Go Back";
            $sent_response = $this->ussd_core(false, $message, 200, "");
            return $sent_response;
        } else{
            $policyNo = '$policyNo';
            $data = array(
                "query" => "query ($policyNo: String!) {pensionAccountInfos (policyNo: $policyNo) { edges { node   {  policyNo policyStartDate product policyStatus memberName oldPolicyNo} }}}",
                "variables" => "{\"policyNo\":\"$pension_customer_member_number\"}"
            );
        }

        $data_string = json_encode($data);
        $curl = curl_init($this->graphql_server_url.'/apex_graphql'); // server
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  // Make it so the data coming back is put into a string
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);  // Insert the data
        // Send the request
        $result = curl_exec($curl);
        // Free up the resources $curl is using
        curl_close($curl);
        $result = json_decode($result, true);


        $node = $result['data']['pensionAccountInfos']['edges'][0];
        $pension_customer_scheme_name = "";
        $pension_customer_start_date = "";
        $pension_customer_status = "";
        $pension_customer_old_member_number = "";
        foreach ($node as $key => $value) {
            $pension_customer_scheme_name = $value['product'];
            $pension_customer_start_date = $value['policyStartDate'];
            $pension_customer_status = $value['policyStatus'];
            $pension_customer_old_member_number = $value['oldPolicyNo'];
            //$pension_customer_new_member_number = $value['policyNo'];
        }
        if($pension_customer_start_date == ""){
            $pension_customer_start_date = "N/A";
        }

        if ($pension_customer_status == 'A') {
            $pension_customer_status = "Active";
        } elseif ($pension_customer_status == 'I') {
            $pension_customer_status = "Inactive";
        }

        $message = "Member Number: " . $pension_customer_old_member_number . "\n Scheme Name: " . $pension_customer_scheme_name . "\n Status: " . $pension_customer_status . "\n Commencement Date: " . $pension_customer_start_date . "\n\n Reply with\n 00. Back";
        $sent_response = $this->ussd_core(false, $message, 200, "");
        return $sent_response;

    }

    public function pensionBalance()
    {

        $userMSISDN = $this->get_put_msisdn();
        $userMobileNumber = substr($userMSISDN, 3);
        $customerNumber = "0" . $userMobileNumber;
        $PensionPolicyInfo = $this->postgres_server_url."/customer?phone_no=" . $customerNumber;
        $ch = curl_init($PensionPolicyInfo);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $policyData = curl_exec($ch);
        curl_close($ch);
        $data_string = json_decode($policyData, true);
        $node = $data_string['customer_details'];
        $pension_customer_member_number = "";
        foreach ($node as $key => $value) {
            $pension_customer_member_number = $value['apex_id'];
        }
        if($pension_customer_member_number == ""){
            $message = "Sorry, you don't have an existing Pension policy with Jubilee Insurance. Please call our Contact Centre on 0709949000 or 0719222111\n\n Reply with: \n 00. Go Back";
            $sent_response = $this->ussd_core(false, $message, 200, "");
            return $sent_response;
        } else{
            $policyNo = '$policyNo';
            $data = array(
                "query" => "query ($policyNo: String!) {pensionAccountInfos (policyNo: $policyNo) { edges { node   {  policyNo policyStartDate product policyStatus memberName oldPolicyNo} }}}",
                "variables" => "{\"policyNo\":\"$pension_customer_member_number\"}"
            );
        }

        $data_string = json_encode($data);
        $curl = curl_init($this->graphql_server_url.'/apex_graphql'); // server
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  // Make it so the data coming back is put into a string
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);  // Insert the data
        // Send the request
        $result = curl_exec($curl);
        // Free up the resources $curl is using
        curl_close($curl);
        $result = json_decode($result, true);


        $node = $result['data']['pensionAccountInfos']['edges'][0];
        $pension_customer_scheme_name = "";
        $pension_customer_start_date = "";
        $pension_customer_status = "";
        $pension_customer_old_member_number = "";
        foreach ($node as $key => $value) {
            $pension_customer_old_member_number = $value['oldPolicyNo'];
            //$pension_customer_new_member_number = $value['policyNo'];
        }
        $PensionBalance = $this->graphql_server_url."/gpnt_statements?member_no=" . $pension_customer_old_member_number;
        //$PensionBalance = "http://10.102.6.177:5000/gpnt_statements?member_no=2PPP130246";
        $ch = curl_init($PensionBalance);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $policyData = curl_exec($ch);
        curl_close($ch);
        $data_string = json_decode($policyData, true);
        $memberBalanceAvailable = array_key_exists('registered_member_value', $data_string) ? true : false;
        if($memberBalanceAvailable){
            //$registered_employee = $data_string['registered_employer_value'];
            $member_value = $data_string['registered_member_value'];
            $employeer_value = $data_string['total_employer_value'];
            $total_member_value = $employeer_value + $member_value;
            $message = "Member Number: " . $pension_customer_old_member_number . "\n Member Value: " . number_format($member_value) . "\n Employer Value: " . number_format($employeer_value) . "\n Total Fund Value: " . number_format($total_member_value) . "\n\n Reply with\n 00. Back";
        }else{
            $message = "Sorry, please call our Contact Centre on 0709949000 or 0719222111 to verify your Pension Balance\n\n Reply with: \n 00. Go Back";
        }
        $sent_response = $this->ussd_core(false, $message, 200, "");
        return $sent_response;
    }

    public function individualMedicalPolicyBenefitsInformation($selection)
    {
        $userMSISDN = $this->get_put_msisdn();
        $userMobileNumber = substr($userMSISDN, 3);
        $customerNumber = "0" . $userMobileNumber;
        $medicalPolicyInfo = $this->postgres_server_url."/customer?phone_no=" . $customerNumber;
        //$medicalPolicyInfo = "http://192.168.52.47:2300/customer?phone_no=0723450405";
        $ch = curl_init($medicalPolicyInfo); // such as http://example.com/example.xml
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $policyData = curl_exec($ch);
        curl_close($ch);
        $data_string = json_decode($policyData, true);
        if (empty($data_string)) {
            $sent_response = $this->end("We encountered a problem. Please try again later", "200");
            echo $sent_response;
        }
        $node = $data_string['customer_details'];
        $policy_count = 0;
        $message = "";
        foreach ($node as $key => $value) {
            $memberNumber = $value['actisure_id'];
            $memberNo = '$memberNo';
            $policyStatus = '$policyStatus';
            $data = array(
                "query" => "query ($memberNo: String! $policyStatus: String!){ medicalInformations (memberNo: $memberNo policyStatus: $policyStatus) { edges { node   {  id memberNo coverLimit scopeOfBenefit policyStatus policyStartDate policyClass renewalDate benefitDistribution} }}}",
                "variables"=>"{\"memberNo\":\"$memberNumber\",\"policyStatus\":\"Live\"}"
            );
            $data_string = json_encode($data, true);
            $curl = curl_init($this->graphql_server_url.'/actisure_fail_over_graphql'); // server
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($data_string))
            );
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  // Make it so the data coming back is put into a string
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);  // Insert the data
            // Send the requestv
            $result = curl_exec($curl);
            // Free up the resources $curl is using
            curl_close($curl);
            $result = json_decode($result, true);
            $node = $result['data']['medicalInformations']['edges'];
            if(!$node) {
                $message = "Your policy is not active. Kindly contact our customer care on 0709949000 or 0719222111\n\n Reply with: \n00: Back";
                $sent_response = $this->ussd_core(false, $message, 200, "");
                return $sent_response;
            } else
            {
                foreach ($node as $key => $value) {
                    $policy_count += 1;
                    $policyType = $value['node']['policyClass'];
                    $policyNumbersAndTypes[$memberNumber] = $policyType;
                    $message = $message . $policy_count . ". " . ucwords(strtolower($policyType)) . "\n";
                }
            }
        }
        $policy_count = 0;
        $medical_customer_policy_number = "";
        foreach ($policyNumbersAndTypes as $policyNumber => $policyType) {
            $policy_count += 1;
            if ($selection == $policy_count) {
                $medical_customer_policy_number = $policyNumber;
            }
        }
        $data = array(
            "query" => "query ($memberNo: String! $policyStatus: String!){ medicalInformations (memberNo: $memberNo policyStatus: $policyStatus) { edges { node   {  id memberNo coverLimit scopeOfBenefit policyStatus policyStartDate policyClass renewalDate benefitDistribution} }}}",
            "variables"=>"{\"memberNo\":\"$memberNumber\",\"policyStatus\":\"Live\"}"
        );
        $data_string = json_encode($data);
        $curl = curl_init($this->graphql_server_url.'/actisure_fail_over_graphql'); // server
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  // Make it so the data coming back is put into a string
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);  // Insert the data
        // Send the request
        $result = curl_exec($curl);
        // Free up the resources $curl is using
        curl_close($curl);
        $result = json_decode($result, true);
        $node = $result['data']['medicalInformations']['edges'];
        $medicalPolicyMemberNumber = "";
        $medicalPolicyCoverLimit = "";
        $medicalPolicyStatus = "";
        $medicalPolicyBalance = "";
        $medicalPolicyClass = "";
        $customer_phonenumber = $this->get_put_msisdn();
        //$customer_phonenumber = 254722742125;
        $displaybenefitsmessage = "";
        $medicalPolicyRenewalDate = "";
        $medicalbenefitDistribution = "";
        $policy_count = 0;
        $benefitsmessage = "";
        $customerMessage = "";
        foreach ($node as $key => $value) {
            $policy_count += 1;
            if ($policy_count > 8)
                continue;
            $medicalPolicyMemberNumber = $value['node']['memberNo'];
            $medicalPolicyCoverLimit = $value['node']['coverLimit'];
            $medicalPolicyClass = $value['node']['policyClass'];
            /*$medicalPolicyUtilizedAmount = $value['scopeOfBenefit'];
            $medicalPolicyBalance = $value['scopeOfBenefit'];*/
            $medicalbenefitDistribution = $value['node']['benefitDistribution'];
            $medicalPolicyRenewalDate = $value['node']['renewalDate'];
            $medicalPolicyScopeOfBenefit = $value['node']['scopeOfBenefit'];
            $benefitsmessage = $benefitsmessage . $policy_count . ". " . "Cover Limit: " . number_format($medicalPolicyCoverLimit) . "\n Scope of Benefit: " . $medicalPolicyScopeOfBenefit . "\n Benefit Type: " . $medicalbenefitDistribution ."\n\n";
            if ($policy_count == 1) {
                $customerMessage = $customerMessage . $policy_count . ". " . "Member Number: ". $medicalPolicyMemberNumber . "\n Cover Limit: " . number_format($medicalPolicyCoverLimit) . "\n Scope of Benefit: " . $medicalPolicyScopeOfBenefit . "\n Benefit Type: " . $medicalbenefitDistribution . "\n\n Other Benefits are sent as SMS \n\n Reply: \n 00: Back";
            }
        }
        if ($this->ci_infobip->sendsms($benefitsmessage, $customer_phonenumber)) {
            $sent_response = $this->ussd_core(false, $benefitsmessage, 200, "");
            return $sent_response;
        } else {
            $sent_response = $this->end("We encountered a problem. Please try again later", "200");
            echo $sent_response;
        }
    }

    public function payMedicalPolicy($selection)
    {
        //global $lifePolicyNumber;
        $policyNumbersAndTypes = array();
        $userMSISDN = $this->get_put_msisdn();
        $userMobileNumber = substr($userMSISDN, 3);
        $customerNumber = "0" . $userMobileNumber;
        $LifePolicyInfo = $this->postgres_server_url."/customer?phone_no=" . $customerNumber;
        //$LifePolicyInfo = "http://192.168.52.47:2300/premiums?phone_no=0711908640";
        $ch = curl_init($LifePolicyInfo);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $policyData = curl_exec($ch);
        curl_close($ch);
        $data_string = json_decode($policyData, true);

        if (empty($data_string)) {
            $sent_response = $this->end("We encountered a problem. Please try again later", "200");
            echo $sent_response;
        }
        $node = $data_string['customer_details'];
        foreach ($node as $key => $value) {
            $memberNumber = $value['actisure_id'];
            $memberNo = '$memberNo';
            $data = array(
                "query" => "query ($memberNo: String!){ medicalInformations (memberNo: $memberNo) { edges { node   {  id memberNo policyStatus policyStartDate scopeOfBenefit policyClass policyStatus coverLimit benefitDistribution  } }}}",
                "variables" => "{\"memberNo\":\"$memberNumber\"}"
            );
            $data_string = json_encode($data);
            $curl = curl_init($this->graphql_server_url.'/actisure_fail_over_graphql'); // server
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($data_string))
            );
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  // Make it so the data coming back is put into a string
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);  // Insert the data
            // Send the request
            $result = curl_exec($curl);
            // Free up the resources $curl is using
            curl_close($curl);
            $result = json_decode($result, true);
            $node = $result['data']['medicalInformations']['edges'][0];
            foreach ($node as $key => $value) {
                $policyType = $value['policyClass'];
                $policyNumbersAndTypes[$memberNumber] = $policyType;
            }
        }
        $policy_count = 0;
        $medical_customer_policy_number = "";
        foreach ($policyNumbersAndTypes as $memberNumber => $policyType) {
            $policy_count += 1;
            if ($selection == $policy_count) {
                $medical_customer_policy_number = $memberNumber;
            }
        }
        $this->medicalCustomerMemberNumber = $medical_customer_policy_number;
        $_SESSION['medicalCustomerMemberNumber'] = $this->medicalCustomerMemberNumber;
        $sent_response = $this->getmenu("44");
        return $sent_response;
    }

    public function medicalPolicyCheckoutRequest($selection)
    {
        $data = array(
            "amount" => $selection,
            "number" => $this->get_put_msisdn(),
            "policynumber" => $this->session->medicalCustomerMemberNumber,
            "lineofbusiness" => "Medical Business",
        );
        if ($this->postToPayment("http://192.168.50.47/mpesaapi/requestcheckout", $data) == true) {
            $sent_response = $this->end("Thank you for using our services", "200");
            echo $sent_response;
        }
    }

    public function emergency_contact_number()
    {
        $message = "0709949000 \n 0719222111";
        $sent_response = $this->ussd_core(false, $message, 200, "");
        return $sent_response;
    }

    public function globalPolicyNumbers($selection)
    {
        $userMSISDN = $this->get_put_msisdn();
        $userMobileNumber = substr($userMSISDN, 3);
        $customerNumber = "0" . $userMobileNumber;
        //$generalPolicyInfo = "http://192.168.52.47:2300/policies?phone_no=".$customerNumber;
        $generalPolicyInfo = $this->postgres_server_url."/policies?phone_no=0722208557";
        $ch = curl_init($generalPolicyInfo);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $policyData = curl_exec($ch);
        curl_close($ch);
        $data_string = json_decode($policyData, true);
        $policies = $data_string['policies'];
        $message = "";
        $policyNumbers = array();
        $count = 0;
        foreach ($policies as $policy) {
            $pol_class = $policy['pol_class'];
            $pol_number = $policy['pol_number'];
            array_push($policyNumbers, $pol_number);
            $count += 1;
            $message = $message . $count . ". " . $pol_number . "\n";
        }
        return $policyNumbers[$selection];
    }

    public function generalPremiumInfo()
    {
        $userMSISDN = $this->get_put_msisdn();
        $userMobileNumber = substr($userMSISDN, 3);
        $customerNumber = "0" . $userMobileNumber;
        $LifePolicyInfo = $this->postgres_server_url."/policies?phone_no=" . $customerNumber;
        //$LifePolicyInfo = "http://192.168.52.47:2300/premiums?phone_no=0711908640";
        $ch = curl_init($LifePolicyInfo);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $policyData = curl_exec($ch);
        curl_close($ch);

        $data_string = json_decode($policyData, true);
        $node = $data_string['policies'];
        $customer_premium_due = "";
        $customer_premium_renewal_date = "";
        $general_customer_policy_number = "";
        foreach ($node as $key => $value) {
            $customer_line_of_business = $value['line_of_business'];
            if ($customer_line_of_business == "GENERAL") {
                $general_customer_policy_number = $value['pol_number'];
            }
        }
        $policyNo = '$policyNo';
        $data = array(
            "query" => "query ($policyNo: String!){policyDetails (policyNo: $policyNo) { edges { node   { netPremium policyNo polClass toDate} }}}",
            "variables" => "{\"policyNo\":\"$general_customer_policy_number\"}"
        );
        $data_string = json_encode($data);
        $curl = curl_init($this->graphql_server_url.'/premia_graphql'); // server
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  // Make it so the data coming back is put into a string
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);  // Insert the data
        // Send the request
        $result = curl_exec($curl);
        // Free up the resources $curl is using
        curl_close($curl);
        $result = json_decode($result, true);
        $node = $result['data']['policyDetails']['edges'][0];
        $general_customer_policy_due = "";
        $general_customer_renewal_date = "";
        foreach ($node as $key => $value) {
            $general_customer_policynumber = $value['policyNo'];
            $general_customer_policy_date = $value['toDate'];
            $general_customer_policy_due = $value['netPremium'];
        }


        //Change date format from datetime to date
        $renewal_date = strtotime($general_customer_policy_date);
        $customer__renewal_date = date('d-m-Y', $renewal_date + 86400);

        $message = "Policy Number: " . $general_customer_policy_number . "\nAmount Payable: " . number_format($general_customer_policy_due) . "\n Renewal Date: " . $customer__renewal_date;
        $sent_response = $this->ussd_core(false, $message, 200, "");
        return $sent_response;
    }

    /*public function contactCustomerCare(){
        $customer_phonenumber = $this->get_put_msisdn();
        $customer_message = 'Kindly contact our customer care on 0709949000 or 0719222111 to be register with our service';
        if ($this->ci_infobip->sendsms($customer_message, $customer_phonenumber)){
            $sent_response=$this->end("Thank you for using our services","200");
            echo $sent_response;
        } else {
            $sent_response=$this->end("We encountered a problem. Please try again later","200");
            echo $sent_response;
        }
    }*/
    public function customerName()
    {
        $userMSISDN = $this->get_put_msisdn();
        $userMobileNumber = substr($userMSISDN, 3);
        $customerNumber = "0" . $userMobileNumber;
        $LifePolicyInfo = $this->postgres_server_url."/customer?phone_no=" . $customerNumber;
        //$LifePolicyInfo = "http://192.168.52.47:2300/premiums?phone_no=0711908640";
        $ch = curl_init($LifePolicyInfo);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $policyData = curl_exec($ch);
        curl_close($ch);

        $data_string = json_decode($policyData, true);
        $node = $data_string['customer_details'];
        $customer_name = "";
        foreach ($node as $key => $value) {
            $customer_line_of_business = $value['line_of_business'];
        }
    }

    public function pensionPerfomance()
    {
        $year1 = "2013";
        $year2 = "2014";
        $year3 = "2015";
        $jubileeInsurance1 = "14.00%";
        $jubileeInsurance2 = "12.00%";
        $jubileeInsurance3 = "8.30%";
        $industryAverage1 = "11.92%";
        $industryAverage2 = "11.04%";
        $industryAverage3 = "7.46%";
        $historicalAverage = "10.48%";

        $message = "Year: $year3, Jubilee Insurance: $jubileeInsurance3, Industry Average: $industryAverage3
                     \n 15 year historical Average = $historicalAverage \n\nMore Details will be sent as SMS \n\n Reply with: \n 00. Back";

        $customerMessage = "Year: $year1, Jubilee Insurance: $jubileeInsurance1, Industry Average: $industryAverage1
                    \n Year: $year2, Jubilee Insurance: $jubileeInsurance2, Industry Average: $industryAverage2
                    \n Year: $year3, Jubilee Insurance: $jubileeInsurance3, Industry Average: $industryAverage3
                    \n 15 year historical Average = $historicalAverage";
        if ($this->ci_infobip->sendsms($customerMessage, $this->get_put_msisdn())) {
            $sent_response = $this->ussd_core(false, $customerMessage, 200, "");
            return $sent_response;
        }

    }

    public function pensionAgentSearch()
    {
        $agentcode = $this->get_put_text();
        $code = '$code';
        $data = array(
            "query" => "query ($code: String!) {agents (code: $code) { edges { node   {  code name } }}}",
            "variables" => "{\"code\":\"$agentcode\"}"
        );
        $data_string = json_encode($data);
        $curl = curl_init($this->graphql_server_url.'/apex_graphql'); // server
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  // Make it so the data coming back is put into a string
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);  // Insert the data
        // Send the request
        $result = curl_exec($curl);
        // Free up the resources $curl is using
        curl_close($curl);
        $result = json_decode($result, true);
        $node = $result['data']['agents']['edges'];
        if(!$node) {
            $message = "Sorry, agent doesn't exist in Jubilee Insurance. Please call our Contact Centre on 0709949000 or 0719222111\n\n Reply with: \n 00. Go Back \n 11. Exit";
            $sent_response = $this->ussd_core(false, $message, 200, "");
            return $sent_response;
        }
        else{
            $node = $result['data']['agents']['edges'][0];
            foreach ($node as $key => $value) {
                $life_agent_name = $value['name'];
                $life_agent_code = $value['code'];
            }
            $message = "Agent Name: " . ucwords(strtolower($life_agent_name)) . "\nAgent Code: " . $life_agent_code . "\n\n Reply with: \n 00. Go Back \n 11. Exit";
            $sent_response = $this->ussd_core(false, $message, 200, "");
            return $sent_response;
        }
    }

    public function lifeAgentSearch()
    {
        $agentcode = $this->get_put_text();
        $code = '$code';
        $data = array(
            "query" => "query ($code: String!) {agents (code: $code) { edges { node   {  code name } }}}",
            "variables" => "{\"code\":\"$agentcode\"}"
        );
        $data_string = json_encode($data);
        $curl = curl_init($this->graphql_server_url.'/isf_graphql'); // server
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  // Make it so the data coming back is put into a string
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);  // Insert the data
        // Send the request
        $result = curl_exec($curl);
        // Free up the resources $curl is using
        curl_close($curl);
        $result = json_decode($result, true);
        $node = $result['data']['agents']['edges'];
        if(!$node) {
            $message = "Sorry, agent doesn't exist in Jubilee Insurance. Please call our Contact Centre on 0709949000 or 0719222111\n\n Reply with: \n 00. Go Back \n 11. Exit";
            $sent_response = $this->ussd_core(false, $message, 200, "");
            return $sent_response;
        }
        else{
            $node = $result['data']['agents']['edges'][0];
            foreach ($node as $key => $value) {
                $life_agent_name = $value['name'];
                $life_agent_code = $value['code'];
            }
            $message = "Agent Name: " . ucwords(strtolower($life_agent_name)) . "\nAgent Code: " . $life_agent_code . "\n\n Reply with: \n 00. Go Back \n 11. Exit";
            $sent_response = $this->ussd_core(false, $message, 200, "");
            return $sent_response;
        }
    }

    public function payLifePolicy($selection)
    {
        //global $lifePolicyNumber;
        $policyNumbersAndTypes = array();
        $userMSISDN = $this->get_put_msisdn();
        $userMobileNumber = substr($userMSISDN, 3);
        $customerNumber = "0" . $userMobileNumber;
        $LifePolicyInfo = $this->postgres_server_url."/policies?phone_no=" . $customerNumber;
        //$LifePolicyInfo = "http://192.168.52.47:2300/premiums?phone_no=0711908640";
        $ch = curl_init($LifePolicyInfo);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $policyData = curl_exec($ch);
        curl_close($ch);
        $data_string = json_decode($policyData, true);

        if (empty($data_string)) {
            $sent_response = $this->end("We encountered a problem. Please try again later", "200");
            echo $sent_response;
        }
        $node = $data_string['policies'];
        foreach ($node as $key => $value) {
            $customer_line_of_business = $value['line_of_business'];
            if ($customer_line_of_business == "LIFE") {
                $policyNumber = $value['pol_number'];
                $policyNo = '$policyNo';
                $data = array(
                    "query" => "query ($policyNo: String!){policyDetails (policyNo: $policyNo) { edges { node   {  policyNo premiumAmount premiumDueDate premiumFrequency totalPremiumPaid nextOutDate issueDate startDate sumAssured termInYears bonus agentName surrenderValue maturityDate policyType loanLimit loanEligible status } }}}",
                    "variables" => "{\"policyNo\":\"$policyNumber\"}"
                );
                $data_string = json_encode($data);
                $curl = curl_init($this->graphql_server_url.'/isf_graphql'); // server
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                        'Content-Type: application/json',
                        'Content-Length: ' . strlen($data_string))
                );
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  // Make it so the data coming back is put into a string
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);  // Insert the data
                // Send the request
                $result = curl_exec($curl);
                // Free up the resources $curl is using
                curl_close($curl);
                $result = json_decode($result, true);
                $node = $result['data']['policyDetails']['edges'][0];
                foreach ($node as $key => $value) {
                    $policyType = $value['policyType'];
                    $policyNumbersAndTypes[$policyNumber] = $policyType;
                }
            }
        }
        $policy_count = 0;
        $life_customer_policy_number = "";
        foreach ($policyNumbersAndTypes as $policyNumber => $policyType) {
            $policy_count += 1;
            if ($selection == $policy_count) {
                $life_customer_policy_number = $policyNumber;
            }
        }
        $this->lifePolicyNumber = $life_customer_policy_number;
        $_SESSION['lifeCustomerPolicyNumber'] = $this->lifePolicyNumber;
        $sent_response = $this->getmenu("44");
        return $sent_response;
    }

    public function lifePolicyCheckoutRequest($selection)
    {
        // global $lifePolicyNumber;
        $data = array(
            "amount" => $selection,
            "number" => $this->get_put_msisdn(),
            "policynumber" => $this->session->lifeCustomerPolicyNumber,
        );
        if ($this->postToPayment("http://192.168.50.47/mpesa_api/requestcheckout", $data) == true) {
            /*$this->Ussd_model->add_session($this->uri->segment(3), $this->get_put_msisdn(), "5", "", "");
            $sent_response = $this->getmenu("5");*/
            $sent_response = $this->end("Thank you for using our services", "200");
            echo $sent_response;
        }
    }

    public function payGeneralPolicy($selection)
    {
        $policyNumbersAndTypes = array();
        $userMSISDN = $this->get_put_msisdn();
        $userMobileNumber = substr($userMSISDN, 3);
        $customerNumber = "0" . $userMobileNumber;
        $LifePolicyInfo = $this->postgres_server_url."/policies?phone_no=" . $customerNumber;
        //$LifePolicyInfo = "http://192.168.52.47:2300/policies?phone_no=0723139922";
        $ch = curl_init($LifePolicyInfo); // such as http://example.com/example.xml
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $policyData = curl_exec($ch);
        curl_close($ch);
        $data_string = json_decode($policyData, true);

        if (empty($data_string)) {
            $sent_response = $this->end("We encountered a problem. Please try again later", "200");
            echo $sent_response;
        }
        $node = $data_string['policies'];
        foreach ($node as $key => $value) {
            $customer_line_of_business = $value['line_of_business'];
            if ($customer_line_of_business == "GENERAL") {
                $policyNumber = $value['pol_number'];
                $policyNo = '$policyNo';
                $data = array(
                    "query" => "query ($policyNo: String!){policyDetails (policyNo: $policyNo) { edges { node   {  fromDate toDate policyNo polClass polSubclass status assuredId } }}}",
                    "variables" => "{\"policyNo\":\"$policyNumber\"}"
                );
                $data_string = json_encode($data);
                $curl = curl_init($this->graphql_server_url.'/premia_graphql'); // server
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                        'Content-Type: application/json',
                        'Content-Length: ' . strlen($data_string))
                );
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  // Make it so the data coming back is put into a string
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);  // Insert the data
                // Send the request
                $result = curl_exec($curl);
                // Free up the resources $curl is using
                curl_close($curl);
                $result = json_decode($result, true);
                $node = $result['data']['policyDetails']['edges'][0];
                foreach ($node as $key => $value) {
                    $policyType = $value['polSubclass'];
                    $policyNumbersAndTypes[$policyNumber] = $policyType;
                }
            }
        }
        $policy_count = 0;
        $life_customer_policy_number = "";
        foreach ($policyNumbersAndTypes as $policyNumber => $policyType) {
            $policy_count += 1;
            if ($selection == $policy_count) {
                $general_customer_policy_number = $policyNumber;
            }
        }
        $this->generalPolicyNumber = $general_customer_policy_number;
        $_SESSION['generalCustomerPolicyNumber'] = $this->generalPolicyNumber;
        $sent_response = $this->getmenu("44");
        return $sent_response;
    }

    public function generalPolicyCheckoutRequest($selection)
    {
        // global $lifePolicyNumber;
        $data = array(
            "amount" => $selection,
            "number" => $this->get_put_msisdn(),
            "policynumber" => $this->session->generalCustomerPolicyNumber,
        );
        if ($this->postToPayment("http://192.168.50.47/mpesa_api/requestcheckout", $data) == true) {
            /*$this->Ussd_models->add_session($this->uri->segment(3), $this->get_put_msisdn(), "5", "", "");
            $sent_response = $this->getmenu("5");*/
            $sent_response = $this->end("Thank you for using our services", "200");
            echo $sent_response;
        }
    }

    public function generalAgentSearch()
    {
        $agentcode = $this->get_put_text();
        $code = '$code';
        $data = array(
            "query" => "query ($code: String!) {agents (code: $code) { edges { node   {  code name } }}}",
            "variables" => "{\"code\":\"$agentcode\"}"
        );
        $data_string = json_encode($data);
        $curl = curl_init($this->graphql_server_url.'/isf_graphql'); // server
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  // Make it so the data coming back is put into a string
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);  // Insert the data
        // Send the request
        $result = curl_exec($curl);
        // Free up the resources $curl is using
        curl_close($curl);
        $result = json_decode($result, true);
        $node = $result['data']['agents']['edges'];
        if(!$node) {
            $message = "Sorry, agent doesn't exist in Jubilee Insurance. Please call our Contact Centre on 0709949000 or 0719222111\n\n Reply with: \n 00. Go Back \n 11. Exit";
            $sent_response = $this->ussd_core(false, $message, 200, "");
            return $sent_response;
        }
        else{
            $node = $result['data']['agents']['edges'][0];
            foreach ($node as $key => $value) {
                $life_agent_name = $value['name'];
                $life_agent_code = $value['code'];
            }
            $message = "Agent Name: " . ucwords(strtolower($life_agent_name)) . "\nAgent Code: " . $life_agent_code . "\n\n Reply with: \n 00. Go Back \n 11. Exit";
            $sent_response = $this->ussd_core(false, $message, 200, "");
            return $sent_response;
        }
    }

    public function medicalAgentSearch($selection)
    {
        $code = '$code';
        $data = array(
            "query" => "query ($code: String!) {agents (code: $code) { edges { node   {  code name phoneNumber type} }}}",
            "variables" => "{\"code\":\"$selection\"}"
        );

        $data_string = json_encode($data);
        $curl = curl_init($this->graphql_server_url.'/actisure_fail_over_graphql'); // server
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  // Make it so the data coming back is put into a string
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);  // Insert the data
        // Send the request
        $result = curl_exec($curl);
        // Free up the resources $curl is using
        curl_close($curl);
        $result = json_decode($result, true);
        $node = $result['data']['agents']['edges'];
        if(!$node) {
            $message = "Sorry, agent doesn't exist in Jubilee Insurance. Please call our Contact Centre on 0709949000 or 0719222111\n\n Reply with: \n 00. Go Back \n 11. Exit";
            $sent_response = $this->ussd_core(false, $message, 200, "");
            return $sent_response;
        }
        else{
            $node = $result['data']['agents']['edges'][0];
            $medical_agent_name = "";
            $medical_agent_code = "";
            $medical_agent_type = "";
            $medical_agent_phoneNumber = "";
            foreach ($node as $key => $value) {
                $medical_agent_name = $value['name'];
                $medical_agent_code = $value['code'];
                $medical_agent_type = $value['type'];
                $medical_agent_phoneNumber = $value['phoneNumber'];
            }
            if ($medical_agent_phoneNumber == null) {
                $medical_agent_phoneNumber = "N/A";
            }
            $message = "Agent Name: " . ucwords(strtolower($medical_agent_name)) . "\nAgent Code: " . $medical_agent_code . "\nPhone Number: " . $medical_agent_phoneNumber . "\nAgent Type: " . $medical_agent_type . "\n\nReply with: \n 00. Go Back.";
            $sent_response = $this->ussd_core(false, $message, 200, "");
            return $sent_response;
        }
    }

    public function generalPremiumDetails()
    {
        $userMSISDN = $this->get_put_msisdn();
        $userMobileNumber = substr($userMSISDN, 3);
        $customerNumber = "0" . $userMobileNumber;
        $generalPolicyInfo = $this->postgres_server_url."/policies?phone_no=".$customerNumber;
//        $generalPolicyInfo = "http://192.168.52.47:2300/policies?phone_no=0722208557";
        $ch = curl_init($generalPolicyInfo);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $policyData = curl_exec($ch);
        curl_close($ch);
        $data_string = json_decode($policyData, true);
        //$node = $data_string['policies'];
        $policies = $data_string['policies'];
        $policy_numbers = array();
        $message = "";
        $policy_count = 0;
        $choices = array();
        foreach ($policies as $policy) {
            $policy_count += 1;
            $pol_class = $policy['pol_class'];
            if ($pol_class == "FIRE") {
                $pol_number = $policy['pol_number'];
                $policyNo = '$policyNo';
                $data = array(
                    "query" => "query ($policyNo: String!) {policyDetails (policyNo: $policyNo) { edges { node   {  fromDate toDate policyNo polClass polSubclass status assuredId } }}}",
                    "variables" => "{\"policyNo\":\"$pol_number\"}"
                );
                $data_string = json_encode($data);
                $curl = curl_init('/premia_graphql'); // server
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                        'Content-Type: application/json',
                        'Content-Length: ' . strlen($data_string))
                );
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  // Make it so the data coming back is put into a string
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);  // Insert the data
                // Send the request
                $result = curl_exec($curl);
                // Free up the resources $curl is using
                curl_close($curl);
                $result = json_decode($result, true);
                $node = $result['data']['policyDetails']['edges'];
                $policy = $node[0]['node'];
                $pol_number = $policy['policyNo'];
                $customer_policy_status = $policy['status'];
                $policy_start_date = $policy['fromDate'];
                $policy_end_date = $policy['toDate'];
                $customer_sub_class = $policy['polSubclass'];
                array_push($policy_numbers, $pol_number);
                array_push($policy_numbers, $pol_number);
                array_push($policy_numbers, $customer_policy_status);
                array_push($policy_numbers, $customer_sub_class);
                array_push($policy_numbers, $policy_start_date);
                array_push($policy_numbers, $policy_end_date);
                $startDate = strtotime($policy_start_date);
                $policy_start_date = date('d-m-Y', $startDate);
                $endDate = strtotime($policy_end_date);
                $policy_end_date = date('d-m-Y', $endDate);
                $message = $policy_count . ". " . $pol_number . "\nPremium Booked: " . $customer_policy_status;
                $choices[$policy_count] = $pol_number;
            }
        }
        $sent_response = $this->ussd_core(false, $message, 200, "");
        return $sent_response;
    }

    public function getAllFirePolicies()
    {
        $userMSISDN = $this->get_put_msisdn();
        $userMobileNumber = substr($userMSISDN, 3);
        $customerNumber = "0" . $userMobileNumber;
        //$PensionPolicyInfo = "http://192.168.52.47:2300/policies?phone_no=".$customerNumber;
        $PensionPolicyInfo = $this->postgres_server_url."/policies?phone_no=0722208557";
        $ch = curl_init($PensionPolicyInfo);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $policyData = curl_exec($ch);
        curl_close($ch);
        $data_string = json_decode($policyData, true);
        //$node = $data_string['policies'];
        $policies = $data_string['policies'];
        $policy_numbers = array();
        $message = "";
        $choices = array();
        $policyNumbers = array();
        $count = 0;

        /*global $policy_data_key_value;*/
        //$final_policy_data_key_value = array();
        foreach ($policies as $policy) {
            $pol_class = $policy['pol_class'];
            if ($pol_class == "FIRE") {
                $pol_number = $policy['pol_number'];
                array_push($policyNumbers, $pol_number);
                $message = $message . $count . ". " . $pol_number . "\n";
                $this->policy_data_key_value[$count] = $pol_number;
            }
        }
        $msisdn = $userMSISDN;
        $session_id = $this->uri->segment(3);
        $sent_response = $this->ussd_core(false, $message, 200, "");
        echo $sent_response;
    }

    public function getAllMotorPolicies()
    {
        $userMSISDN = $this->get_put_msisdn();
        $userMobileNumber = substr($userMSISDN, 3);
        $customerNumber = "0" . $userMobileNumber;
        $PensionPolicyInfo = $this->postgres_server_url."/policies?phone_no=" . $customerNumber;
        //$PensionPolicyInfo = "http://192.168.52.47:2300/policies?phone_no=0722208557";
        $ch = curl_init($PensionPolicyInfo);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $policyData = curl_exec($ch);
        curl_close($ch);
        $data_string = json_decode($policyData, true);
        //$node = $data_string['policies'];
        $policies = $data_string['policies'];
        $policy_numbers = array();
        $message = "";
        $choices = array();
        $policyNumbers = array();
        $count = 0;

        /*global $policy_data_key_value;*/
        //$final_policy_data_key_value = array();
        foreach ($policies as $policy) {
            $pol_class = $policy['pol_class'];
            if ($pol_class == "MOTOR") {
                //++$count;
                $pol_number = $policy['pol_number'];
                array_push($policyNumbers, $pol_number);
                $message = $message . $count . ". " . $pol_number . "\n";
                $this->policy_data_key_value[$count] = $pol_number;
            }
        }
        $sent_response = $this->ussd_core(false, $message, 200, "");
        echo $sent_response;
    }

    public function getAllAccidentPoliciesPremium()
    {
        $userMSISDN = $this->get_put_msisdn();
        $userMobileNumber = substr($userMSISDN, 3);
        $customerNumber = "0" . $userMobileNumber;
        //$PensionPolicyInfo = "http://192.168.52.47:2300/policies?phone_no=".$customerNumber;
        $PensionPolicyInfo = $this->postgres_server_url."/policies?phone_no=0722208557";
        $ch = curl_init($PensionPolicyInfo);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $policyData = curl_exec($ch);
        curl_close($ch);
        $data_string = json_decode($policyData, true);
        //$node = $data_string['policies'];
        $policies = $data_string['policies'];
        $policy_numbers = array();
        $message = "";
        $choices = array();
        $policyNumbers = array();
        $count = 0;

        /*global $policy_data_key_value;*/
        //$final_policy_data_key_value = array();
        foreach ($policies as $policy) {
            $pol_class = $policy['pol_class'];
            if ($pol_class == "ACCIDENT") {
                $pol_number = $policy['pol_number'];
                array_push($policyNumbers, $pol_number);
                $count += 1;
                $message = $message . $count . ". " . $pol_number . "\n";
                $this->policy_data_key_value[$count] = $pol_number;
            }
        }
        $msisdn = $userMSISDN;
        $session_id = $this->uri->segment(3);
        $sent_response = $this->ussd_core(false, $message, 200, "");

        //$sent_response=$this->generalAccidentPolicyInfo();
        echo $sent_response;
    }

    public function getGeneralMenuList()
    {
        $userMSISDN = $this->get_put_msisdn();
        $userMobileNumber = substr($userMSISDN, 3);
        $customerNumber = "0" . $userMobileNumber;
        $LifePolicyInfo = $this->postgres_server_url."/policies?phone_no=" . $customerNumber;
        //$LifePolicyInfo = "http://192.168.52.47:2300/policies?phone_no=0723139922";
        $ch = curl_init($LifePolicyInfo); // such as http://example.com/example.xml
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $policyData = curl_exec($ch);
        curl_close($ch);
        $data_string = json_decode($policyData, true);

        if (empty($data_string)) {
            $sent_response = $this->end("We encountered a problem. Please try again later", "200");
            echo $sent_response;
        }
        $node = $data_string['policies'];
        $policy_count = 0;
        $message = "";
        foreach ($node as $key => $value) {
            $customer_line_of_business = $value['line_of_business'];
            if ($customer_line_of_business == "GENERAL") {
                $policyNumber = $value['pol_number'];
                $policyNo = '$policyNo';
                $data = array(
                    "query" => "query ($policyNo: String!){policyDetails (policyNo: $policyNo) { edges { node   { policyNo polClass polSubclass netPremium  } }}}",
                    "variables" => "{\"policyNo\":\"$policyNumber\"}"
                );
                $data_string = json_encode($data);
                $curl = curl_init($this->graphql_server_url.'/premia_graphql'); // server
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                        'Content-Type: application/json',
                        'Content-Length: ' . strlen($data_string))
                );
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  // Make it so the data coming back is put into a string
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);  // Insert the data
                // Send the request
                $result = curl_exec($curl);
                // Free up the resources $curl is using
                curl_close($curl);
                $result = json_decode($result, true);
                $node = $result['data']['policyDetails']['edges'][0];
                foreach ($node as $key => $value) {
                    $policy_count += 1;
                    $policyType = $value['polSubclass'];
                    if ($policyType == "PRIVATE CAR - SCHEME POLICY" || $policyType == "PRIVATE CAR - BINDER POLICY") {
                        $policyType = "PRIVATE CAR";
                    } elseif ($policyType == "MOTOR GENERAL CARTAGE BINDER") {
                        $policyType = "MOTOR GENERAL CARTAGE";
                    } elseif ($policyType == "MOTOR CYCLES-BINDER") {
                        $policyType = "MOTOR CYCLES";
                    } elseif ($policyType == "MOTOR COMMERCIAL - SCHEME POLI" || $policyType == "MOTOR COMMERCIAL - BINDER") {
                        $policyType = "MOTOR COMMERCIAL";
                    } elseif ($policyType == "LADY JUBILEE - SCHEME POLICY") {
                        $policyType = "LADY JUBILEE";
                    } elseif ($policyType == "TRAVEL INSURANCE-BINDER") {
                        $policyType = "TRAVEL INSURANCE";
                    } elseif ($policyType == "MARINE CARGO-BINDER") {
                        $policyType = "MARINE CARGO";
                    } elseif ($policyType == "DOMESTIC PACKAGE-BINDER") {
                        $policyType = "DOMESTIC PACKAGE";
                    } elseif ($policyType == "GROUP PERSONAL ACCIDENT BINDER") {
                        $policyType = "GROUP PERSONAL ACCIDENT";
                    }
                    $policyNumbersAndTypes[$policyNumber] = $policyType;
                    $message = $message . $policy_count . ". " . ucwords(strtolower($policyType)) . "\n";
                }
            }
        }
        $sent_response = $this->ussd_core(false, $message . "\n\n Reply with\n 00. Back", 200, "");
        return $sent_response;
    }

    public function getMedicalMenuList()
    {
        $userMSISDN = $this->get_put_msisdn();
        $userMobileNumber = substr($userMSISDN, 3);
        $customerNumber = "0" . $userMobileNumber;
        $medicalPolicyInfo = $this->postgres_server_url."/customer?phone_no=" . $customerNumber;
        //$medicalPolicyInfo = "http://192.168.52.47:2300/customer?phone_no=0723450405";
        $ch = curl_init($medicalPolicyInfo); // such as http://example.com/example.xml
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $policyData = curl_exec($ch);
        curl_close($ch);
        $data_string = json_decode($policyData, true);
        if (empty($data_string)) {
            $sent_response = $this->end("We encountered a problem. Please try again later", "200");
            echo $sent_response;
        }
        $node = $data_string['customer_details'];
        $policy_count = 0;
        $message = "";
        foreach ($node as $key => $value) {
            $memberNumber = $value['actisure_id'];
            $memberNo = '$memberNo';
            $data = array(
                "query" => "query ($memberNo: String!){ medicalInformations (memberNo: $memberNo) { edges { node   {  id memberNo policyStatus policyStartDate scopeOfBenefit policyClass policyStatus coverLimit benefitDistribution  } }}}",
                "variables" => "{\"memberNo\":\"$memberNumber\"}"
            );
            $data_string = json_encode($data, true);
            $curl = curl_init($this->graphql_server_url.'/actisure_fail_over_graphql'); // server
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($data_string))
            );
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  // Make it so the data coming back is put into a string
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);  // Insert the data
            // Send the requestv
            $result = curl_exec($curl);
            // Free up the resources $curl is using
            curl_close($curl);
            $result = json_decode($result, true);
            $node = $result['data']['medicalInformations']['edges'][0];
            foreach ($node as $key => $value) {
                $policy_count += 1;
                $policyType = $value['policyClass'];
                $policyNumbersAndTypes[$memberNumber] = $policyType;
                $message = "Reply with: \n" . $message . $policy_count . ". " . $policyType . "\n 00. Back";
            }
        }
        $sent_response = $this->ussd_core(false, $message, 200, "");
        return $sent_response;
    }

    public function individualMedicalPolicyInformation($selection)
    {
        $userMSISDN = $this->get_put_msisdn();
        $userMobileNumber = substr($userMSISDN, 3);
        $customerNumber = "0" . $userMobileNumber;
        $medicalPolicyInfo = $this->postgres_server_url."/customer?phone_no=" . $customerNumber;
        //$medicalPolicyInfo = "http://192.168.52.47:2300/customer?phone_no=0723450405";
        $ch = curl_init($medicalPolicyInfo); // such as http://example.com/example.xml
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $policyData = curl_exec($ch);
        curl_close($ch);
        $data_string = json_decode($policyData, true);
        if (empty($data_string)) {
            $sent_response = $this->end("We encountered a problem. Please try again later", "200");
            echo $sent_response;
        }
        $node = $data_string['customer_details'];
        $policy_count = 0;
        $message = "";
        foreach ($node as $key => $value) {
            $memberNumber = $value['actisure_id'];
            $memberNo = '$memberNo';
            $policyStatus = '$policyStatus';
            $data = array(
                "query" => "query ($memberNo: String! $policyStatus: String!){ medicalInformations (memberNo: $memberNo policyStatus: $policyStatus) { edges { node   {  id memberNo policyStatus policyStartDate scopeOfBenefit policyClass policyStatus coverLimit benefitDistribution } }}}",
                "variables"=>"{\"memberNo\":\"$memberNumber\",\"policyStatus\":\"Live\"}"
            );
            $data_string = json_encode($data, true);
            $curl = curl_init($this->graphql_server_url.'/actisure_fail_over_graphql'); // server
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($data_string))
            );
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  // Make it so the data coming back is put into a string
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);  // Insert the data
            // Send the requestv
            $result = curl_exec($curl);
            // Free up the resources $curl is using
            curl_close($curl);
            $result = json_decode($result, true);

            /*            else if (empty($result['data']['medicalInformations']['edges'])){
                            $sent_response = $this->end("60 days are past since the last policy Renewal Date. Kindly contact our customer care on 0709949000 or 0719222111", "200");
                            echo $sent_response;
                        }*/
            $node = $result['data']['medicalInformations']['edges'];
            if(!$node) {
                $message = "Your policy is not active. Kindly contact our customer care on 0709949000 or 0719222111\n\n Reply with: \n00: Back";
                $sent_response = $this->ussd_core(false, $message, 200, "");
                return $sent_response;
            } else {
                foreach ($node as $key => $value) {
                    $policy_count += 1;
                    $policyType = $value['node']['policyClass'];
                    $policyNumbersAndTypes[$memberNumber] = $policyType;
                    $message = $message . $policy_count . ". " . ucwords(strtolower($policyType)) . "\n";
                }
            }
        }
        $policy_count = 0;
        $medical_customer_policy_number = "";
        foreach ($policyNumbersAndTypes as $policyNumber => $policyType) {
            $policy_count += 1;
            if ($selection == $policy_count) {
                $medical_customer_policy_number = $policyNumber;
            }
        }
        $data = array(
            "query" => "query ($memberNo: String! $policyStatus: String!){ medicalInformations (memberNo: $memberNo policyStatus: $policyStatus last: 1) { edges { node   {  id memberNo coverLimit scopeOfBenefit policyStatus policyStartDate policyClass renewalDate benefitDistribution } }}}",
            "variables"=>"{\"memberNo\":\"$memberNumber\",\"policyStatus\":\"Live\"}"
        );
        $data_string = json_encode($data);
        $curl = curl_init($this->graphql_server_url.'/actisure_fail_over_graphql'); // server
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  // Make it so the data coming back is put into a string
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);  // Insert the data
        // Send the request
        $result = curl_exec($curl);
        // Free up the resources $curl is using
        curl_close($curl);
        $result = json_decode($result, true);
        $node = $result['data']['medicalInformations']['edges'];
        $message = "";
        foreach ($node as $key => $value) {
            $medicalPolicyMemberNumber = $value['node']['memberNo'];
            $medicalPolicyCoverLimit = $value['node']['coverLimit'];
            $medicalPolicyCommencementDate = $value['node']['policyStartDate'];
            $medicalPolicyStartDate = $value['node']['policyStartDate'];
            $medicalPolicyRenewalDate = $value['node']['renewalDate'];
            $medicalPolicyStatus = $value['node']['policyStatus'];
            $medicalPolicyClass = $value['node']['policyClass'];
            //Change of policy status to Active and Inactive
            if ($medicalPolicyStatus == "Live") {
                $medicalPolicyStatus = "Active";
            }

            $today = date('d-m-Y');
            $queryDate = date('d-m-Y', strtotime($today));
            $policyRenewalDate = strtotime(stripslashes($medicalPolicyRenewalDate));
            $policyTerminationDate = date("d-m-Y", strtotime(' + 60 days', $policyRenewalDate));

            if ($medicalPolicyClass == "Corporate" || $medicalPolicyClass == "SME" && $medicalPolicyStatus = "Active") {
                $message = $message . "Member Number: " . $medicalPolicyMemberNumber . "\n Status: " . $medicalPolicyStatus . "\n Commencement Date: " . $medicalPolicyCommencementDate .
                    "\n Start Date: " . $medicalPolicyStartDate . "\n\n Reply with: \n00: Back";
            } elseif ($medicalPolicyClass == "J-Care" && $medicalPolicyStatus = "Active") {
                $message = $message . "Member Number: " . $medicalPolicyMemberNumber . "\n Status: " . $medicalPolicyStatus . "\n Commencement Date: " . $medicalPolicyCommencementDate .
                    "\n Start Date: " . $medicalPolicyStartDate . "\n Renewal Date: " . $medicalPolicyRenewalDate . "\n\n Reply with: \n00: Back";
            } elseif ($medicalPolicyClass == "J-Care" && $queryDate > $policyTerminationDate) {
                $message = "60 days are past since the last policy Renewal Date. Kindly contact our customer care on 0709949000 or 0719222111\n\n Reply with: \n00: Back";
            }

        }
        $sent_response = $this->ussd_core(false, $message, 200, "");
        return $sent_response;
    }

    public function getPensionMenuList()
    {
        $userMSISDN = $this->get_put_msisdn();
        $userMobileNumber = substr($userMSISDN, 3);
        $customerNumber = "0" . $userMobileNumber;
        $pensionsPolicyInfo = $this->postgres_server_url."/policies?phone_no=" . $customerNumber;
        $ch = curl_init($pensionsPolicyInfo); // such as http://example.com/example.xml
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $policyData = curl_exec($ch);
        curl_close($ch);
        $data_string = json_decode($policyData, true);

        if (empty($data_string)) {
            $sent_response = $this->end("Sorry, you don't have an existing Pension policy with Jubilee Insurance. Please call our Contact Centre on 0709949000 or 0719222111", "200");
            echo $sent_response;
        }
        $node = $data_string['policies'];
        $policy_count = 0;
        $message = "";
        foreach ($node as $key => $value) {
            $customer_line_of_business = $value['line_of_business'];
            if ($customer_line_of_business == "PENSIONS") {
                $policyNumber = $value['pol_number'];
                $policyNo = '$policyNo';
                $data = array(
                    "query" => "query ($policyNo: String!){pensionAccountInfos (policyNo: $policyNo) { edges { node   {policyNo policyStartDate product policyStatus memberName oldPolicyNo } }}}",
                    "variables" => "{\"policyNo\":\"$policyNumber\"}"
                );
                $data_string = json_encode($data);
                $curl = curl_init($this->graphql_server_url.'/apex_graphql'); // server
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                        'Content-Type: application/json',
                        'Content-Length: ' . strlen($data_string))
                );
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  // Make it so the data coming back is put into a string
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);  // Insert the data
                // Send the request
                $result = curl_exec($curl);
                // Free up the resources $curl is using
                curl_close($curl);
                $result = json_decode($result, true);
                $node = $result['data']['pensionAccountInfos']['edges'][0];
                if(!$node){
                    $message = "Sorry, you don't have an existing Pension policy with Jubilee Insurance. Please call our Contact Centre on 0709949000 or 0719222111\n\n Reply with: \n 00. Go Back";
                } else{
                    foreach ($node as $key => $value) {
                        $policy_count += 1;
                        $policyType = $value['policyType'];
                        $policyNumbersAndTypes[$policyNumber] = $policyType;
                        $message = $message . $policy_count . ". " . ucwords(strtolower($policyType)) . "\n";
                    }
                }

            }

        }
        $sent_response = $this->ussd_core(false, $message . "\n\n Reply with\n 00. Back", 200, "");
        return $sent_response;
    }

    public function getLifeMenuList()
    {
        $userMSISDN = $this->get_put_msisdn();
        $userMobileNumber = substr($userMSISDN, 3);
        $customerNumber = "0" . $userMobileNumber;
        $LifePolicyInfo = $this->postgres_server_url."/policies?phone_no=" . $customerNumber;
        //$LifePolicyInfo = "http://192.168.52.47:2300/policies?phone_no=0723139922";
        $ch = curl_init($LifePolicyInfo); // such as http://example.com/example.xml
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $policyData = curl_exec($ch);
        curl_close($ch);
        $data_string = json_decode($policyData, true);

        if (empty($data_string)) {
            $sent_response = $this->end("We encountered a problem. Please try again later", "200");
            echo $sent_response;
        }
        $node = $data_string['policies'];
        $policy_count = 0;
        $message = "";
        foreach ($node as $key => $value) {
            $customer_line_of_business = $value['line_of_business'];
            if ($customer_line_of_business == "LIFE") {
                $policyNumber = $value['pol_number'];
                $policyNo = '$policyNo';
                $data = array(
                    "query" => "query ($policyNo: String!){policyDetails (policyNo: $policyNo) { edges { node   {  policyNo premiumAmount premiumDueDate premiumFrequency totalPremiumPaid nextOutDate issueDate startDate sumAssured termInYears bonus agentName surrenderValue maturityDate policyType loanLimit loanEligible status } }}}",
                    "variables" => "{\"policyNo\":\"$policyNumber\"}"
                );
                $data_string = json_encode($data);
                $curl = curl_init($this->graphql_server_url.'/isf_graphql'); // server
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                        'Content-Type: application/json',
                        'Content-Length: ' . strlen($data_string))
                );
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  // Make it so the data coming back is put into a string
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);  // Insert the data
                // Send the request
                $result = curl_exec($curl);
                // Free up the resources $curl is using
                curl_close($curl);
                $result = json_decode($result, true);
                $node = $result['data']['policyDetails']['edges'][0];
                if(!$node){
                    $message = "Sorry, you don't have an existing Pension policy with Jubilee Insurance. Please call our Contact Centre on 0709949000 or 0719222111\n\n Reply with: \n 00. Go Back";
                } else{
                    foreach ($node as $key => $value) {
                        $policy_count += 1;
                        $policyType = $value['policyType'];
                        $policyNumbersAndTypes[$policyNumber] = $policyType;
                        $message = $message . $policy_count . ". " . ucwords(strtolower($policyType)) . "\n";
                    }
                }

            }

        }
        $sent_response = $this->ussd_core(false, $message . "\n\n Reply with\n 00. Back", 200, "");
        return $sent_response;
    }

    public function payMedicalWithPolicyNumber()
    {
        $session_id = $this->uri->segment(3);
        $msisdn = $this->get_put_msisdn();
        $userMSISDN = $this->get_put_msisdn();
        $userMobileNumber = substr($userMSISDN, 3);
        $customerNumber = "0" . $userMobileNumber;
        //$generalPolicyInfo = "http://192.168.52.47:2300/policies?phone_no=0723450405";
        $generalPolicyInfo = $this->postgres_server_url."/policies?phone_no=" . $customerNumber;
        $ch = curl_init($generalPolicyInfo);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $policyData = curl_exec($ch);
        curl_close($ch);
        $data_string = json_decode($policyData, true);
        $policies = $data_string['policies'];
        foreach ($policies as $policy) {
            $pol_class = $policy['pol_class'];
            if ($pol_class == "Corporate") {
                $medicalPolicyNumber = $policy['pol_number'];
            }
        }
        $data = array(
            "amount" => $this->get_put_text(),
            "number" => $this->get_put_msisdn(),
            "policynumber" => $medicalPolicyNumber
        );
        if ($this->postToPayment("http://192.168.50.47/mpesa_api/requestcheckout", $data) == true) {
            /*$this->Ussd_models->add_session($session_id, $msisdn, "5", "", "");
            $sent_response = $this->getmenu("5");*/
            $sent_response = $this->end("Thank you for using our services", "200");
            echo $sent_response;
        }


    }

    public function payGeneralWithPolicyNumber($policyNumber, $customerPayingAmount)
    {
        $session_id = $this->uri->segment(3);
        $msisdn = $this->get_put_msisdn();
        $userMSISDN = $this->get_put_msisdn();
        $userMobileNumber = substr($userMSISDN, 3);
        $customerNumber = "0" . $userMobileNumber;
        $generalPolicyInfo = $this->postgres_server_url."/policies?phone_no=0723450405";
        $ch = curl_init($generalPolicyInfo);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $policyData = curl_exec($ch);
        curl_close($ch);
        $data_string = json_decode($policyData, true);
        $policies = $data_string['policies'];
        foreach ($policies as $policy) {
            $pol_class = $policy['pol_class'];
            if ($pol_class == "FIRE") {
                $generalPolicyNumber = $policy['pol_number'];
            } elseif ($pol_class == "MOTOR") {
                $generalPolicyNumber = $policy['pol_number'];
            }
        }
        $data = array(
            "amount" => $customerPayingAmount,
            "number" => $this->get_put_msisdn(),
            "policynumber" => $policyNumber,
        );
        if ($this->postToPayment("http://192.168.50.47/mpesa_api/requestcheckout", $data) == true) {
            $this->Ussd_models->add_session($session_id, $msisdn, "5", "", "");
            $sent_response = $this->getmenu("5");
            echo $sent_response;
        }
    }

    public function individualPolicyNumbersUnderClass($policyClass)
    {
        $agentcode = $this->get_put_text();
        //make payment against life policy
        $userMSISDN = $this->get_put_msisdn();
        $userMobileNumber = substr($userMSISDN, 3);
        $customerNumber = "0" . $userMobileNumber;
        $LifePolicyInfo = $this->postgres_server_url."/policies?phone_no=0722208557";
        $ch = curl_init($LifePolicyInfo);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $policyData = curl_exec($ch);
        curl_close($ch);
        $data_string = json_decode($policyData, true);
        $policies = $data_string['policies'];
        $policy_numbers = array();
        $message = "";
        $policy_count = 0;
        $choices = array();
        foreach ($policies as $policy) {
            $policy_count += 1;
            $pol_class = $policy['pol_class'];
            if ($pol_class == $policyClass) {
                $pol_number = $policy['pol_number'];
                array_push($policy_numbers, $pol_number);
                $message = $message . $policy_count . ". " . $pol_number . "\n";
                $choices[$policy_count] = $pol_number;
            }
        }
        $sent_response = $this->ussd_core(false, $message, 200, "");
        return $sent_response;
    }

    public function individualGeneralAccidentPolicyInfo($polNumber)
    {
        $sent_response = $this->ussd_core(false, $polNumber, 200, "");
        $policyNo = '$policyNo';
        $data = array(
            "query" => "query ($policyNo: String!) {policyDetails (policyNo: $policyNo) { edges { node   {  fromDate toDate policyNo polClass polSubclass status assuredId } }}}",
            "variables" => "{\"policyNo\":\"$polNumber\"}"
        );
        $data_string = json_encode($data);
        $curl = curl_init($this->graphql_server_url.'/premia_graphql'); // server
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  // Make it so the data coming back is put into a string
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);  // Insert the data
        // Send the request
        $result = curl_exec($curl);
        // Free up the resources $curl is using
        curl_close($curl);
        $result = json_decode($result, true);
        $node = $result['data']['policyDetails']['edges'];
        $count = 0;
        $policy_numbers = array();
        $message = "";
        $policy = $node[0]['node'];
        $pol_number = $policy['policyNo'];
        $customer_policy_status = $policy['status'];
        $policy_start_date = $policy['fromDate'];
        $policy_end_date = $policy['toDate'];
        $customer_sub_class = $policy['polSubclass'];

        $startDate = strtotime($policy_start_date);
        $policy_start_date = date('d-m-Y', $startDate);
        $endDate = strtotime($policy_end_date);
        $policy_end_date = date('d-m-Y', $endDate);
        $message = $pol_number . "\nStatus: " . $customer_policy_status . "\n Sub Class: " . ucwords(strtolower($customer_sub_class)) . "\n Start Date: " . $policy_start_date . "\nEnd Date: " . $policy_end_date . "\n";
        $sent_response = $this->ussd_core(false, $message, 200, "");
        return $sent_response;
    }
}
